export default {
  init() {
    $('.js-section-slider-container').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');

      new Swiper(this, {
        slidesPerView: 1.15,
        simulateTouch: false,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $pagination,
          clickable: true
        },
        breakpoints: {
          640: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1260: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    $('.js-section-slider-news').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');

      new Swiper(this, {
        slidesPerView: 2.1,
        slidesPerGroup: 1,
        simulateTouch: false,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $pagination,
          clickable: true
        },
        breakpoints: {
          640: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 4,
            slidesPerGroup: 4,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    new Swiper('.js-article-slider', {
      wrapperClass: 'js-slider-wrapper',
      slideClass: 'js-slider-slide',
      speed: 1000,
      loop: true,
      breakpointsInverse: true,
      autoplay: {
        delay: 3000
      },
      navigation: {
        prevEl: '.js-slider-prev',
        nextEl: '.js-slider-next'
      },
      breakpoints: {
        0: {
          slidesPerView: 1.1,
          spaceBetween: 10
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        768: {
          slidesPerView: 2.3,
          spaceBetween: 20
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      }
    });

    new Swiper('.js-intro-relay', {
      wrapperClass: 'js-slider-wrapper',
      slideClass: 'js-slider-slide',
      loop: true,
      breakpointsInverse: true,
      autoplay: {
        delay: 3000
      },
      navigation: {
        prevEl: '.js-slider-prev',
        nextEl: '.js-slider-next'
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        640: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 10
        }
      }
    });

    if ($(window).width() < 1260){
      new Swiper('.js-moders-slider', {
        wrapperClass: 'js-moders-wrapper',
        slideClass: 'js-moders-slide',
        // loop: true,
        breakpointsInverse: true,
        pagination: {
          el: '.js-moders-pagination',
        },
        breakpoints: {
          0: {
            loop: true,
            slidesPerView: 1.3,
            spaceBetween: 30
          },

          380: {
            loop: true,
            slidesPerView: 1.4,
            spaceBetween: 30
          },

          420: {
            loop: true,
            slidesPerView: 2,
            spaceBetween: 30
          },

          480: {
            loop: true,
            slidesPerView: 2,
            spaceBetween: 30
          },
          640: {
            loop: true,
            slidesPerView: 3,
            spaceBetween: 30
          },
          768: {
            loop: true,
            slidesPerView: 3,
            spaceBetween: 30
          },
          1024: {
            loop: true,
            slidesPerView: 4,
            spaceBetween: 30
          }
        }
      });
    }

    $('.js-section-slider-libs').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');

      new Swiper(this, {
        slidesPerView: 1.2,
        slidesPerGroup: 1,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $pagination,
          clickable: true
        },
        breakpoints: {
          640: {
            slidesPerView: 2,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          768: {
            slidesPerView: 2.4,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 4,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    $('.js-section-slider-libs-new').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');
      const $counterMobile = $wrapper.find('.js-slider-bullets');

      new Swiper(this, {
        slidesPerView: 1.2,
        slidesPerGroup: 1,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $counterMobile
        },
        breakpoints: {
          640: {
            slidesPerView: 2,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          768: {
            slidesPerView: 2.4,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 4,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    $('.js-section-experts').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');
      const $counterMobile = $wrapper.find('.js-slider-bullets');

      new Swiper(this, {
        slidesPerView: 1.1,
        slidesPerGroup: 1,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $counterMobile
        },
        breakpoints: {
          640: {
            slidesPerView: 2,
            slidesPerGroup: 1,
          },

          768: {
            slidesPerView: 1.5,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          992: {
            slidesPerView: 2,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 2,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1260: {
            slidesPerView: 2.5,
            slidesPerGroup: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });


    $('.js-expert-slider').each(function () {
      const $wrapper = $(this).closest('.js-expert-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');

      new Swiper(this, {
        slidesPerView: 1.1,
        slidesPerGroup: 1,
        simulateTouch: false,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $pagination,
          clickable: true
        },
        breakpoints: {
          480: {
            slidesPerView: 2,
            slidesPerGroup: 2,
          },

          640: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },
          1260: {
            slidesPerView: 4,
            slidesPerGroup: 4,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    $('.js-section-slider-reviews').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');

      new Swiper(this, {
        slidesPerView: 1.15,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $pagination,
          clickable: true
        },
        breakpoints: {
          640: {
            slidesPerView: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 1,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    $('.js-section-slider-partners').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $counterMobile = $wrapper.find('.js-slider-bullets');

      new Swiper(this, {
        slidesPerView: 3,
        slidesPerGroup: 3,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $counterMobile
        },
        breakpoints: {
          480: {
            slidesPerView: 3,
            slidesPerGroup: 3
          },

          640: {
            slidesPerView: 5,
            slidesPerGroup: 5
          },

          768: {
            slidesPerView: 4,
            slidesPerGroup: 4
          },

          1024: {
            slidesPerView: 5,
            slidesPerGroup: 5,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1260: {
            slidesPerView: 6,
            slidesPerGroup: 6,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });

    if ($(window).width() < 1260) {
      new Swiper('.js-scs-slider', {
        slidesPerView: 1.07,
        spaceBetween:10,
        breakpointInverse: true,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        pagination: {
          el: '.js-slider-pagination'
        },
        breakpoints: {
          480: {
            slidesPerView:1.4,
            spaceBetween:15
          },

          640: {
            slidesPerView:2,
            spaceBetween:15
          },
          768: {
            slidesPerView:3,
            spaceBetween:15
          },
          1170: {
            slidesPerView:4,
            spaceBetween:15
          }
        }
      });

      new Swiper('.js-levels-slider', {
        slidesPerView: 1.1,
        spaceBetween:15,
        breakpointInverse: true,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        pagination: {
          el: '.js-slider-pagination'
        },
        breakpoints: {
          640: {
            slidesPerView:2,
            spaceBetween:15
          },
          1170: {
            slidesPerView:4,
            spaceBetween:15
          }
        }
      });
    }

    $('.js-section-slider-vocabularies').each(function () {
      const $wrapper = $(this).closest('.js-section-slider');
      const $prevEl = $wrapper.find('.js-slider-prev');
      const $nextEl = $wrapper.find('.js-slider-next');
      const $counter = $wrapper.find('.js-slider-counter');
      const $pagination = $wrapper.find('.js-slider-pagination');

      new Swiper(this, {
        slidesPerView: 1.15,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        breakpointInverse: true,
        navigation: {
          prevEl: $prevEl,
          nextEl: $nextEl
        },
        pagination: {
          el: $pagination,
          clickable: true
        },
        breakpoints: {
          640: {
            slidesPerView: 2,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1024: {
            slidesPerView: 3,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          },

          1260: {
            slidesPerView: 4,
            pagination: {
              el: $counter,
              type: 'fraction',
              formatFractionCurrent(number) {
                return (number < 10) ? '0' + number : number;
              },
              formatFractionTotal(number) {
                return (number < 10) ? '0' + number : number;
              }
            },
          }
        }
      });
    });
  }
}
