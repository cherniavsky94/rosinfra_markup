export default {
  $el: $('.js-page-header'),
  isSticked: false,
  className: 'is-sticky-header',

  init() {
    const $page = $('.page');
    const $window = $(window);
    const headerHeight = this.getHeight();

    $window.on('load scroll resize', () => {
      let scrollTop = $window.scrollTop();

      if (scrollTop > 1) {
        $page.addClass(this.className);
        this.isSticked = true;
      } else {
        $page.removeClass(this.className);
        this.isSticked = false;
      }
    });
    // $('.js-page-header').stick_in_parent();
  },

  getHeight() {
    return this.$el.outerHeight();
  }
}
