export default {
  instance: undefined,
  init() {
    if ($(window).width() < 1260) {
      this.instance = new Swiper('.js-advants-slider', {
        slidesPerView: 1,
        breakpointInverse: true,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        pagination: {
          el: '.js-slider-pagination'
        },
        breakpoints: {
          480: {
            slidesPerView: 2
          },
          1024: {
            slidesPerView: 3
          }
        }
      });
    }

    if ($(window).width() < 1260) {
      this.instance = new Swiper('.js-levels-slider', {
        slidesPerView: 1.1,
        spaceBetween:15,
        breakpointInverse: true,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        pagination: {
          el: '.js-slider-pagination'
        },
        breakpoints: {
          640: {
            slidesPerView:2,
            spaceBetween:15
          },
          1170: {
            slidesPerView:4,
            spaceBetween:15
          }
        }
      });
    }

    if ($(window).width() < 1260) {
      this.instance = new Swiper('.js-services-slider', {
        slidesPerView:1.07,
        spaceBetween:10,
        slidesPerView: 1,
        breakpointInverse: true,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        pagination: {
          el: '.js-slider-pagination'
        },
        breakpoints: {
          480: {
            slidesPerView:1.4,
            spaceBetween:15
          },
          640: {
            slidesPerView:2,
            spaceBetween:15
          },
          768: {
            slidesPerView:3,
            spaceBetween:15
          },
          1170: {
            slidesPerView:4,
            spaceBetween:15
          }
        }
      });
    }
  }
}

