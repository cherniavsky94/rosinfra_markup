$(function () {
  $('.js-select-custom').each(function () {
    const $select = $(this),
    $header = $select.find('.js-select-custom-header'),
    $input = $select.find('.js-select-custom-input'),
    $list = $select.find('.js-select-custom-list'),
    // $checkbox = $list.find('input').filter((id, item) => item.checked),
    $selected = $select.find('.js-select-custom-selected'),
    $apply = $select.find('.js-select-custom-apply'),
    placeholder = $input.attr('placeholder'),
    options = $select.data(),
    $count = $select.find('.ui-select-custom__selected-count'),
    $reset = $select.find('.ui-select-custom__reset-btn'),
    $selectedName = $select.find('.js-selected-element-name'),
    $selectedEl = $select.find('.js-selected-element'),
    $actions = $select.find('.ui-select-custom__selected-actions'),
    $countVal = $count.find('.ui-badge');

    $header.on('click', function () {
      $input.attr('placeholder', options.placeholder);
      $select.addClass('is-opened');

      // const bottomLine = $(window).scrollTop() + $(window).height();
      // const selectHeight = $header.outerHeight() + $list.outerHeight();

      // if (($select.offset().top + selectHeight) > bottomLine) {
      //   $select.addClass('is-up');
      // }
    });

    $input.on('input', () => search($list, $input));

    $reset.on('click', function(event){
      event.preventDefault();
      event.stopPropagation();

      $select.removeClass('is-opened');
      $input.attr('placeholder', placeholder);
      $input.attr('value', '');

      $selectedName.html('');
      $actions.css('display','none');
    });

    $selectedEl.on('click', function(){
      let $placeholderCur = $(this).html();
      $select.removeClass('is-opened');
      $input.attr('placeholder', $placeholderCur);
      $selectedName.html($placeholderCur);
      $input.attr('value', $placeholderCur);
      $actions.css('display','flex');
    });

    // $(document).on('click', function(event){
    //   if(!$(event.target).closest('.js-select-custom').length) {
    //     $select.removeClass('is-opened');
    //     $input.attr('placeholder', placeholder);
    //   }
    // });

    $(document).mouseup(function (e) {
      var container = $select;
      if (container.has(e.target).length === 0){
          container.removeClass('is-opened is-up');

          const checkboxCount = $list.find('input').filter((id, item) => item.checked).length;

          $select.removeClass('is-opened');
          $input.attr('placeholder', placeholder);

          // if(checkboxCount > 0){
          //   $count.css('display','flex');
          //   $countVal.html(checkboxCount);
          // } else {
          //   $count.css('display','none');
          // }

          let $selectedNameL = $selectedName.html();

          if($selectedNameL.length <= 0){
            $actions.css('display','none');
          } else {
            $actions.css('display','flex');
          }
      }
    });

    $(document).ready(function(){
      let $selectedNameL = $selectedName.html();

      if($selectedNameL.length <= 0){
        $actions.css('display','none');
      } else {
        $actions.css('display','flex');
      }
    });

    $selected.children().each(function () {
      const $group = $(this),
        $input = $group.find('.js-selected-custom-input'),
        $list = $group.find('.js-selected-custom-list');

      $input.on('input', () => search($list, $input));
    });


    function search($list, $input) {
      $list.children().each(function () {
        const $item = $(this),
          title = $item.find('.input-control__label').text();

        if (title.toLowerCase().indexOf($input.val().toLowerCase()) > -1) {
          $item.show();
        } else {
          $item.hide();
        }
      });
    }
  });

});
