import axios from 'axios';

export default {
  props: ['placeholderSearch', 'placeholderSearchChildren', 'placeholder', 'url', 'param', 'list', 'child', 'theme', 'paramChild'],

  data() {
    return {
      isOpened: false,
      selected: [],
      selectedApply: [],
      selectedChildren: {},
      data: {},
      search: '',
      searchChildren: {}
    }
  },

  computed: {
    placeholderActive() {
      return this.isOpened ? this.placeholderSearch : this.placeholder;
    }
  },

  watch: {
    selected(value, oldValue) {
      let id = null;

      if (oldValue.length > value.length) {
        for (let i = 0; i < oldValue.length; i++) {
          if (!value[i]) {
            id = oldValue[i];
          }
        }
      }

      if (id) {
        let index = this.selectedApply.indexOf(id);
        this.selectedApply.splice(index, 1);

        this.selectedChildren[id] = [];
      }
    },

    selectedChildren: {
      handler(value) {
        // const selected = [];
        //
        // for (let key in value) {
        //   for (let i = 0; i < value[key].length; i++) {
        //     selected.push(value[key][i]);
        //   }
        // }
        //
        // this.$root.models[this.child] = selected;
      },
      deep: true
    },

    list(value, oldValue) {
      for (let key in value) {
        this.$set(this.searchChildren, key, '');
      }
    }
  },

  methods: {
    remove(key) {
      const index = this.selected.indexOf(key);

      this.selected.splice(index, 1);
      this.selectedChildren[key] = [];
      this.searchChildren[key] = '';
      this.$root.models[this.param] = this.selectedApply = this.selected;
    },

    apply() {
      const _this = this;
      const requests = [];

      if (this.child) {
        for (let i = 0; i < this.selected.length; i++) {
          requests.push(axios.get(`${this.url}?${this.param}[0]=${this.selected[i]}`));
        }

        if (!requests.length) {
          _this.$root[_this.child] = [];
          _this.$root.models[_this.paramChild] = [];
        }

        axios.all(requests).then(axios.spread(function () {
          const dataChild = {};

          for (let i = 0; i < _this.selected.length; i++) {
            // _this.$set(_this.data, _this.selected[i], arguments[i].data);
            //
            // const selected = _this.selectedChildren[_this.selected[i]] || [];
            // _this.$set(_this.selectedChildren, _this.selected[i], selected);

            for (let key in arguments[i].data) {
              dataChild[key] = arguments[i].data[key];
            }
          }

          _this.$root[_this.child] = dataChild;
          _this.$root.models[_this.param] = _this.selectedApply = _this.selected;
        }));
      } else {
        _this.$root.models[_this.param] = _this.selectedApply = _this.selected;
      }

      _this.search = '';
      _this.isOpened = false;
    },

    searchChildrenValue(e, id) {
      this.searchChildren[id] = e.target.value;
    },

    open() {
      setTimeout(() => {
        this.isOpened = true;
      }, 100);
    }
  },

  mounted() {
    $(document).on('click', (event) => {
      if(!$(event.target).closest('.ui-select-custom.is-opened').length) {
        this.search = '';
        this.isOpened = false;
      }
    });
  },

  updated() {
    $(this.$refs.group).data('opened', false).on('click', '.ui-select-custom-list__title', function () {
      const $group = $(this).closest('.ui-select-custom-list__group');
      if (!$group.data('opened')) {
        $group.addClass('is-opened').find('.ui-select-custom-list__body').slideDown(function () {
          $group.data('opened', true);
        });
      } else {
        $group.removeClass('is-opened').find('.ui-select-custom-list__body').slideUp(function () {
          $group.data('opened', false);
        });
      }
    });
  },

  template: `
    <div class="ui-select-custom" :class="[theme, { 'is-opened' : isOpened }]">
      <div class="ui-select-custom__header" @click="open">
        <input class="ui-input ui-input_type_text ui-input_size_md" :class="{'is-selected' : selectedApply.length && !isOpened}" type="text" :placeholder="placeholderActive" v-model="search" />
        <span class="ui-select-custom__selected-count" v-if="selectedApply.length && !isOpened">Выбрано элементов: <span class="ui-badge ui-badge_type_danger ui-badge_size_sm">{{ selectedApply.length }}</span></span>
      </div>
      <div class="ui-select-custom__body">
        <div class="ui-select-custom__dropdown">
          <div class="ui-select-custom__scrollbar js-scrollbar">
            <div class="ui-select-custom__content">
              <div class="ui-select-custom__item" v-for="item, key in list" :key="key" v-if="item.toLowerCase().indexOf(search.toLowerCase()) > -1">
                <label class="ui-control">
                  <input class="ui-control__input" type="checkbox" :value="key" v-model="selected" />
                  <span class="ui-control__toggle"></span>
                  <span class="ui-control__label">{{ item }}</span>
                </label>
              </div>
            </div>
          </div>
          <div class="ui-select-custom__footer">
            <button class="ui-button ui-button_type_primary ui-button_size_md is-round ui-button_block" @click="apply">
              <span class="ui-button__title">Применить фильтр</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  `
}
