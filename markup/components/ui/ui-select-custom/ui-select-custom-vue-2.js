import axios from 'axios';

export default {
  props: {
    // 'placeholderSearchChildren',
    // 'url',
    // 'param',
    // 'list',
    // 'child',
    // 'theme',
    list: {
      type: Object,
      default: () => {}
    },

    child: {
      type: Array,
      default: () => []
    },

    placeholder: {
      type: String,
      default: 'Выберите варианты'
    },

    placeholderSearch: {
      type: String,
      default: 'Поиск вариантов'
    },

    theme: {
      type: String,
      default: ''
    },

    selected: {
      type: Array,
      default: () => []
    }
  },

  data() {
    return {
      isOpened: false,
      selectedApply: [],
      selectedChildren: {},
      data: {},
      search: '',
      searchChildren: {}
    }
  },

  computed: {
    placeholderActive() {
      return this.isOpened ? this.placeholderSearch : this.placeholder;
    }
  },

  watch: {
    selected(value, oldValue) {
      let id = null;

      if (oldValue.length > value.length) {
        for (let i = 0; i < oldValue.length; i++) {
          if (!value[i]) {
            id = oldValue[i];
          }
        }
      }

      if (id) {
        let index = this.selectedApply.indexOf(id);
        this.selectedApply.splice(index, 1);

        this.selectedChildren[id] = [];
      }
      this.$emit('set:custom-select', this.$vnode.key, value);
    },

    selectedChildren: {
      handler(value) {
        const selected = [];

        for (let key in value) {
          for (let i = 0; i < value[key].length; i++) {
            selected.push(value[key][i]);
          }
        }

        this.$root.models[this.child] = selected;
      },
      deep: true
    },

    list(value, oldValue) {
      for (let key in value) {
        this.$set(this.searchChildren, key, '');
      }
    }
  },

  methods: {
    remove(key) {
      const index = this.selected.indexOf(key);

      this.selected.splice(index, 1);
      this.selectedChildren[key] = [];
      this.searchChildren[key] = '';
      this.$root.models[this.param] = this.selectedApply = this.selected;
    },

    apply() {
      this.$emit('set:custom-select', this.$vnode.key, this.selected);

      this.search = '';
      this.isOpened = false;
    },

    clear() {
      this.selected = [];
      this.search = '';
    },

    searchChildrenValue(e, id) {
      this.searchChildren[id] = e.target.value;
    },

    open() {
      setTimeout(() => {
        this.isOpened = true;
      }, 100);
    }
  },

  mounted() {
    $(document).on('click', (event) => {
      if(!$(event.target).closest('.ui-select-custom.is-opened').length) {
        this.search = '';
        this.isOpened = false;
      }
    });
  },

  updated() {
    $(this.$refs.group).data('opened', false).on('click', '.ui-select-custom-list__title', function () {
      const $group = $(this).closest('.ui-select-custom-list__group');
      if (!$group.data('opened')) {
        $group.addClass('is-opened').find('.ui-select-custom-list__body').slideDown(function () {
          $group.data('opened', true);
        });
      } else {
        $group.removeClass('is-opened').find('.ui-select-custom-list__body').slideUp(function () {
          $group.data('opened', false);
        });
      }
    });
  },

  template: `
    <div class="ui-select-custom" :class="[theme, { 'is-opened' : isOpened }]">
      <div class="ui-select-custom__header" @click="open">
        <input class="ui-input ui-input_type_text ui-input_size_md" :class="{'is-selected' : selectedApply.length && !isOpened}" type="text" :placeholder="placeholderActive" v-model="search" />
        <span class="ui-select-custom__selected-count" v-if="selected.length && !isOpened">Выбрано: <span class="ui-badge ui-badge_type_danger ui-badge_size_sm">{{ selected.length }}</span></span>
        <span class="ui-select-custom__selected-reset" v-if="selected.length && !isOpened" @click.stop="clear"></span>
      </div>
      <div class="ui-select-custom__body">
        <div class="ui-select-custom__dropdown">
          <div class="ui-select-custom__scrollbar js-scrollbar">
            <div class="ui-select-custom__content">
              <div class="ui-select-custom__item" v-for="item, key in list" :key="key" v-if="item.toLowerCase().indexOf(search.toLowerCase()) > -1">
                <label class="ui-control">
                  <input class="ui-control__input" type="checkbox" :value="key" v-model="selected" />
                  <span class="ui-control__toggle"></span>
                  <span class="ui-control__label">{{ item }}</span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="ui-select-custom-list" v-if="child">
          <div class="ui-select-custom-list__group" ref="group" v-for="id, key in selected" :key="key" v-if="selectedApply.indexOf(id) > -1">
            <div class="ui-select-custom-list__header">
              <div class="ui-select-custom-list__title">{{ list[id] }}</div>
              <div class="ui-select-custom-list__delete" title="Удалить" @click="remove(id)">Удалить</div>
            </div>
            <div class="ui-select-custom-list__body">
              <div class="ui-select-custom-list__search">
                <input class="ui-input ui-input_type_primary ui-input_size_md" type="text" :placeholder="placeholderSearchChildren" @input="searchChildrenValue($event, id)"" />
              </div>
              <div class="ui-select-custom-list__content js-list">
                <div class="ui-select-custom-list__list js-list-container">
                  <div class="ui-select-custom-list__item" v-for="item, key in data[id]" :key="key" v-if="item.toLowerCase().indexOf(searchChildren[id].toLowerCase()) > -1">
                    <label class="ui-control">
                      <input class="ui-control__input" type="checkbox" :value="key" v-model="selectedChildren[id]" />
                      <span class="ui-control__toggle"></span>
                      <span class="ui-control__label">{{ item }}</span>
                    </label>
                  </div>
                </div>
                <div class="js-list-toggle-parent">
                  <span class="ui-toggle-link js-list-toggle">
                    <span class="ui-label ui-label_type_muted ui-label_size_md">Показать еще</span>
                    <i class="fe fe-chevron-down"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
}
