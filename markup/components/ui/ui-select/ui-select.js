import select2 from 'select2';

import axios from 'axios';

axios.get('http://localhost:100/api/digest/initiative-all').then((response) => {
  // console.log(response)
});

$('.ui-select select').each(function () {
  const $select = $(this),
    data = $select.data(),
    isModal = $select.closest('.js-modal').length;

  let defaultOptions = {
    width: '100%',
    dropdownAutoWidth: true,
    // dropdownParent: isModal ? $select.parent() : $(document.body)
    dropdownParent: $select.parent(),
    minimumResultsForSearch: -1
  };

  let options = Object.assign(defaultOptions, data);

  $select.select2(options);
});
