export default {
  options: {
    activeClassName: 'is-active'
  },

  init() {
    const _this = this;
    const _window = $(window);
    const $sections = $('.js-menu-sections-list').children();
    const $links = $('.js-menu-section-link');
    const pageHeaderHeight = APP.Components.PageHeader.getHeight();

    $(document).on('click', '.js-menu-section-link', function (event) {
      event.preventDefault();

      const $link = $(this),
            $item = $link.parent(),
            $destination = $($link.attr('href'));

      if ($destination.length) {
        APP.Plugins.ScrollTo($destination);
        APP.Components.Accordion.open($destination);
      }
    });

    _window.on('load scroll', function () {
      let scrollTop = _window.scrollTop() + pageHeaderHeight;

      if (_window.scrollTop() + _window.height() >= $(document).height()){
        let $activeLink = $('.menu-sections__item').last().children();
        $activeLink.parent().addClass(_this.options.activeClassName).siblings().removeClass(_this.options.activeClassName);
      } else {
        $sections.each(function () {
          const $section = $(this);
          const sectionSrollTop = $section.offset().top - 1;

          if (scrollTop >= sectionSrollTop) {
            const sectionId = $section.attr('id');

            const $activeLink = $links.filter(function () {
              return $(this).attr('href') === `#${sectionId}`;
            });

            $activeLink.parent().addClass(_this.options.activeClassName).siblings().removeClass(_this.options.activeClassName);
          }
        });
      }
    });
  }
}
