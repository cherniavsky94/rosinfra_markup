var data = {sectionDirections: {
  business: [
    {
      href: '#',
      img: {
        src: '__static__img/content/categories/experts.svg'
      },
      title: 'Экспертная помощь в online режиме\n',
      caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
    },
    {
      href: '#',
      img: {
        src: '__static__img/content/categories/analitycs.svg'
      },
      title: 'Доступ к лучшим практикам и аналитике',
      caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
    },
    {
      href: '#',
      img: {
        src: '__static__img/content/categories/pickup-projects.svg'
      },
      title: 'Подбор перспективных проектов ГЧП',
      caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
    },
    {
      href: '#',
      img: {
        src: '__static__img/content/categories/access.svg'
      },
      title: 'Доступ к заказчикам и продвижение услуг',
      caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
    }
  ],

    government: [
      {
        href: '#',
        img: {
          src: '__static__img/content/categories/experts.svg'
        },
        title: 'Экспертная помощь в online режиме\n',
        caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
      },
      {
        href: '#',
        img: {
          src: '__static__img/content/categories/analitycs.svg'
        },
        title: 'Доступ к лучшим практикам и аналитике',
        caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
      },
      {
        href: '#',
        img: {
          src: '__static__img/content/categories/expertise.svg'
        },
        title: 'Экспертиза и содействие в запуске проектов ',
        caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
      },
      {
        href: '#',
        img: {
          src: '__static__img/content/categories/investors.svg'
        },
        title: 'Привлечение инвесторов и партеров',
        caption: 'Помощь в подготовке и заключения ведущих специалистов рынка'
      }
    ]
}};
