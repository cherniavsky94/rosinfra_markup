export default {
  init() {
    new Swiper('.js-categories-slider', {
      slidesPerView: 1,
      simulateTouch: false,
      wrapperClass: 'js-slider-wrapper',
      slideClass: 'js-slider-slide',
      breakpointInverse: true,
      pagination: {
        el: '.js-slider-pagination'
      },
      breakpoints: {
        480: {
          slidesPerView: 2
        },
        768: {
          slidesPerView: 3
        },
        1024: {
          slidesPerView: 4
        }
      }
    });
  }
}
