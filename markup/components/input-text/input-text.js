export default {
  init() {
    $('.js-input-text-date').flatpickr({
      dateFormat: 'd.m.Y'
    });

    $('.js-input-text-time').flatpickr({
      enableTime: true,
      noCalendar: true,
      dateFormat: "H:i",
    });

    $('.js-input-text-date-new').flatpickr({
      dateFormat: 'd.m.Y',
      inline: true
    });
  }
}
