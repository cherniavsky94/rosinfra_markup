export default {
  options: {
    className: 'is-search'
  },

  init() {
    $(document).on('click', '.js-search-toggle', () => this.toggle());
  },

  open() {
    $('body').addClass(this.options.className);
    APP.Browser.Scroll.locked();
  },

  close() {
    $('body').removeClass(this.options.className);
    APP.Browser.Scroll.unlocked();
  },

  toggle() {
    this.isOpened() ? this.close() : this.open();
  },

  isOpened() {
    return $('body').hasClass(this.options.className);
  }
}
