export default {
  options: {
    openedClassName: 'is-opened'
  },

  init() {
    const $menus = $('.js-menu');

    $menus.each((id, el) => {
      const $menu = $(el),
            $toggle = $menu.find('.js-menu-toggle');

      $toggle.on('click', () => {
        $menu.toggleClass(this.options.openedClassName);
      });
    });

    $(document).on('click', (event) => {
      const $target = $(event.target);

      if (!$target.closest('.js-menu').length) {
        $menus.removeClass(this.options.openedClassName);
      }
    });
  }
}
