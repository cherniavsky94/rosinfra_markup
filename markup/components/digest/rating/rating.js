export default {
  Chart: {
    init(file) {
      Papa.parse(file, {
        download: true,
        complete(results) {
          const data = [];

          for (let i = 0; i < results.data.length; i++) {
            if (results.data[i][0] !== '') {
              let stepValue = results.data[i][4];
              let stepType = '';

              if (stepValue < 0) {
                stepType = 'is-danger';
              } else if (stepValue >= 0) {
                stepType = 'is-success';
              }

              data.push({
                title: results.data[i][1],
                level: parseFloat(results.data[i][2].replace(/,/g, '.')),
                color: results.data[i][8]
              });
            }
          }

          new Vue({
            el: '.js-digest-rating-chart',

            data: {
              data: data,
              showStart: 16
            },

            methods: {
              showMore() {
                this.showStart = this.data.length
              }
            },

            mounted() {
              const prev = this.$refs.sliderPrev;
              const next = this.$refs.sliderNext;

              new Swiper(this.$refs.slider, {
                slidesPerView: 'auto',
                wrapperClass: 'js-slider-wrapper',
                slideClass: 'js-slider-slide',
                navigation: {
                  prevEl: prev,
                  nextEl: next
                }
              })
            }
          });
        }
      });
    }
  },

  List: {
    init(file) {
      Papa.parse(file, {
        download: true,
        complete(results) {
          const data = [];

          for (let i = 0; i < results.data.length; i++) {
            if (results.data[i][0] !== '') {
              let stepValue = results.data[i][4];
              let stepType = '';

              if (stepValue < 0) {
                stepType = 'is-danger';
              } else if (stepValue >= 0) {
                stepType = 'is-success';
              }

              data.push({
                number: results.data[i][0],
                title: results.data[i][1],
                level: results.data[i][2],
                place: results.data[i][3],
                step: {
                  value: stepValue,
                  type: stepType
                },
                count: results.data[i][5],
                legal: results.data[i][6],
                icon: results.data[i][7],
                note: results.data[i][9]
              });
            }
          }

          new Vue({
            el: '.js-digest-rating-list',

            data: {
              data: data,
              showStart: 16
            },

            methods: {
              showMore() {
                this.showStart = this.data.length
              }
            }
          });
        }
      });
    }
  }
}
