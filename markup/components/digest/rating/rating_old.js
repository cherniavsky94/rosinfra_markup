export default {
  Chart: {
    init(file) {
      Papa.parse(file, {
        download: true,
        complete(results) {
          const data = [];

          for (let i = 0; i < results.data.length; i++) {
            if (results.data[i][0] !== '') {
              data.push({
                title: results.data[i][1],
                level: parseFloat(results.data[i][2].replace(/,/g, '.')),
                color: results.data[i][25]
              });
            }
          }

          new Vue({
            el: '.js-digest-rating-chart-old',

            data: {
              data: data,
              showStart: 16
            },

            methods: {
              showMore() {
                this.showStart = this.data.length
              }
            },

            mounted() {
              const prev = this.$refs.sliderPrev;
              const next = this.$refs.sliderNext;

              new Swiper(this.$refs.slider, {
                slidesPerView: 'auto',
                wrapperClass: 'js-slider-wrapper',
                slideClass: 'js-slider-slide',
                navigation: {
                  prevEl: prev,
                  nextEl: next
                }
              })
            }
          });
        }
      });
    }
  },

  List: {
    init(file) {
      Papa.parse(file, {
        download: true,
        complete(results) {
          const data = [];

          for (let i = 0; i < results.data.length; i++) {
            if (results.data[i][0] !== '') {
              data.push({
                number: results.data[i][0],
                title: results.data[i][1],
                integral: results.data[i][2],
                perfomance: results.data[i][3],
                kpi: results.data[i][4],
                legal: results.data[i][5],
                expirience: results.data[i][6],
                icon: results.data[i][24],
                props: [results.data[i][7], results.data[i][8], results.data[i][9], results.data[i][10], results.data[i][11], results.data[i][12], results.data[i][13], results.data[i][14], results.data[i][15], results.data[i][16], results.data[i][17], results.data[i][18], results.data[i][19], results.data[i][20], results.data[i][21], results.data[i][22], results.data[i][23]],
                isOpened: false
              });
            }
          }

          new Vue({
            el: '.js-digest-rating-list-old',

            data: {
              data: data,
              showStart: 16
            },

            methods: {
              showMore() {
                this.showStart = this.data.length
              }
            }
          });
        }
      });
    }
  }
}
