export default {
  init() {
    $('.js-select2').each(function () {
      const $select = $(this),
            data = $select.data(),
            isModal = $select.closest('.js-modal').length;

      let defaultOptions = {
        width: '100%',
        dropdownAutoWidth: true,
        dropdownParent: isModal ? $select.parent() : $(document.body)
      };

      let options = Object.assign(defaultOptions, data);

      $select.select2(options);
    });
  }
}
