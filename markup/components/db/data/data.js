var data = {db: {
  footerMenu: [
    [
      {
        href: '#',
        title: 'Бизнесу'
      },
      {
        href: '#',
        title: 'Власти'
      },
      {
        href: '#',
        title: 'Помощь экспертов'
      }
    ],
    [
      {
        href: '#',
        title: 'Реализуемые проекты'
      },
      {
        href: '#',
        title: 'Планируемые проекты'
      },
      {
        href: '#',
        title: 'Компании и услуги'
      }
    ],
    [
      {
        href: '#',
        title: 'Аналитический центр'
      },
      {
        href: '#',
        title: 'Библиотека'
      },
      {
        href: '#',
        title: 'ГЧП-Академия'
      }
    ],
    [
      {
        href: '#',
        title: 'Публичные партнеры'
      },
      {
        href: '#',
        title: 'ГЧП-рейтинг регионов'
      }
    ]
  ]
}};
