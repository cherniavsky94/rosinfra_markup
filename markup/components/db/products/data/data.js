var data = {products: [{
  heading: 'Крупнейшая база инфраструктурных проектов',
  img: {
    src: '__static__img/content/products/1.jpg'
  },
  button: {
    href: '#',
    title: 'База проектов',
    type: 'is-danger is-round',
    size: 'is-average'
  }
}, {
  heading: 'Онлайн-консультации по вопросам подготовки проектов',
  img: {
    src: '__static__img/content/products/2.jpg'
  },
  button: {
    href: '#',
    title: 'Помощь экспертов',
    type: 'is-danger is-round',
    size: 'is-average'
  }
}, {
  heading: 'Актуальная аналитика по теме инфраструктуры и ГЧП',
  img: {
    src: '__static__img/content/products/3.jpg'
  },
  button: {
    href: '#',
    title: 'Аналитический центр',
    type: 'is-danger is-round',
    size: 'is-average'
  }
}, {
  heading: 'Обзор инвестиционного потенциала публичных партнёров',
  img: {
    src: '__static__img/content/products/4.jpg'
  },
  button: {
    href: '#',
    title: 'Публичные партнеры',
    type: 'is-danger is-round',
    size: 'is-average'
  }
}, {
  heading: 'Получение практических знаний в дистанционном формате',
  img: {
    src: '__static__img/content/products/5.jpg'
  },
  button: {
    title: 'ГЧП Академия',
    type: 'is-danger is-round is-disabled',
    size: 'is-average'
  },
  hint: 'В разработке'
}, {
  heading: 'Поиск инвесторов на любой этап реализации проекта',
  img: {
    src: '__static__img/content/products/6.jpg'
  },
  button: {
    href: '#',
    title: 'Биржа проектных инициатив',
    type: 'is-danger is-round',
    size: 'is-average'
  }
}]};
