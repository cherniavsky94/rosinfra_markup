var data = {services: {
  default: [{
    type: 'is-service',
    heading: 'Консультация ведущих экспертов рынка',
    caption: 'Проработка вопроса экспертом с предоставлением письменного ответа или консультации в Skype',
    img: {
      src: '__static__img/content/services/1.jpg'
    },
    button: {
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average',
      modal: 'become-expert'
    }
  }, {
    type: 'is-service',
    heading: 'Подготовка экспертного заключения ',
    caption: '«Второе мнение» на финансовую модель, проект соглашения и другую проектную документацию',
    img: {
      src: '__static__img/content/services/2.jpg'
    },
    button: {
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average',
      modal: 'become-expert'
    }
  }, {
    type: 'is-service',
    heading: 'Оценка проектных инициатив',
    caption: 'Оценим планируемый проект на успешность его реализации с применением механизма ГЧП',
    img: {
      src: '__static__img/content/services/3.jpg'
    },
    button: {
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average',
      modal: 'become-expert'
    }
  }],

  others: [{
    type: 'is-service is-small',
    heading: 'Консультирование по вопросам антимонопольного регулирования',
    img: {
      src: '__static__img/content/services/4.jpg'
    },
    button: {
      href: '#',
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average'
    }
  }, {
    type: 'is-service is-small',
    heading: 'Анализ источников финансирование и поиск инвестора для финансирования проекта (в т.ч. Использование бюджетных средств)',
    img: {
      src: '__static__img/content/services/5.jpg'
    },
    button: {
      href: '#',
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average'
    }
  }, {
    type: 'is-service is-small',
    heading: 'Проверка проекта заявки/конкурсного предложения для участия в конкурсе',
    img: {
      src: '__static__img/content/services/6.jpg'
    },
    button: {
      href: '#',
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average'
    }
  }, {
    type: 'is-service is-small',
    heading: 'Консультирование по вопросам антимонопольного регулирования',
    img: {
      src: '__static__img/content/services/4.jpg'
    },
    button: {
      href: '#',
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average'
    }
  }, {
    type: 'is-service is-small',
    heading: 'Анализ источников финансирование и поиск инвестора для финансирования проекта (в т.ч. Использование бюджетных средств)',
    img: {
      src: '__static__img/content/services/5.jpg'
    },
    button: {
      href: '#',
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average'
    }
  }, {
    type: 'is-service is-small',
    heading: 'Проверка проекта заявки/конкурсного предложения для участия в конкурсе',
    img: {
      src: '__static__img/content/services/6.jpg'
    },
    button: {
      href: '#',
      title: 'Узнать больше',
      type: 'is-danger is-round',
      size: 'is-average'
    }
  }]
}};
