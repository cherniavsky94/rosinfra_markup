var data = {countries: [
    {
      href: '#',
      icon: '__static__img/content/flags/rf.svg',
      title: 'Российская Федерация',
      more: 'Перейти к информации о стране',
      partners: {
        value: '335',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/ukraine.svg',
      title: 'Украина',
      more: 'Перейти к информации о стране',
      partners: {
        value: '154',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/md.svg',
      title: 'Молдавия',
      more: 'Перейти к информации о стране',
      partners: {
        value: '123',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/rm.svg',
      title: 'Румыния',
      more: 'Перейти к информации о стране',
      partners: {
        value: '56',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/bg.svg',
      title: 'Болгария',
      more: 'Перейти к информации о стране',
      partners: {
        value: '112',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/ab.svg',
      title: 'Албания',
      more: 'Перейти к информации о стране',
      partners: {
        value: '36',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/gc.svg',
      title: 'Греция',
      more: 'Перейти к информации о стране',
      partners: {
        value: '43',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/tc.svg',
      title: 'Турция',
      more: 'Перейти к информации о стране',
      partners: {
        value: '12',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/gz.svg',
      title: 'Грузия',
      more: 'Перейти к информации о стране',
      partners: {
        value: '22',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/am.svg',
      title: 'Армения',
      more: 'Перейти к информации о стране',
      partners: {
        value: '123',
        label: 'Публичных партнёров'
      }
    },
    {
      href: '#',
      icon: '__static__img/content/flags/azb.svg',
      title: 'Азербайджан',
      more: 'Перейти к информации о стране',
      partners: {
        value: '212',
        label: 'Публичных партнёров'
      }
    }
]};
