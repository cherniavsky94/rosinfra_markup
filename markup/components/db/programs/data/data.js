var data = {programs: [{
  heading: 'Финансовое моделирование',
  img: {
    src: '__static__img/content/products/1.jpg'
  },
  properties: {
    date: 'Гибкие сроки'
  },
  tags: [{
    title: 'Новичок'
  }],
  button: {
    href: '#',
    title: 'Узнать больше',
    type: 'is-white',
    size: 'is-sm'
  }
}, {
  heading: 'Соглашение о ГЧП',
  img: {
    src: '__static__img/content/products/2.jpg'
  },
  properties: {
    date: 'С 25 марта'
  },
  tags: [{
    title: 'Специалист'
  }],
  button: {
    href: '#',
    title: 'Узнать больше',
    type: 'is-white',
    size: 'is-sm'
  }
}, {
  heading: 'Основы ГЧП',
  img: {
    src: '__static__img/content/products/3.jpg'
  },
  properties: {
    date: '21-23 апреля'
  },
  tags: [{
    title: 'Специалист'
  }],
  button: {
    href: '#',
    title: 'Узнать больше',
    type: 'is-white',
    size: 'is-sm'
  }
}, {
  heading: 'Финансовое моделирование',
  img: {
    src: '__static__img/content/products/1.jpg'
  },
  properties: {
    date: 'Гибкие сроки'
  },
  tags: [{
    title: 'Новичок'
  }],
  button: {
    href: '#',
    title: 'Узнать больше',
    type: 'is-white',
    size: 'is-sm'
  }
}, {
  heading: 'Соглашение о ГЧП',
  img: {
    src: '__static__img/content/products/2.jpg'
  },
  properties: {
    date: 'С 25 марта'
  },
  tags: [{
    title: 'Специалист'
  }],
  button: {
    href: '#',
    title: 'Узнать больше',
    type: 'is-white',
    size: 'is-sm'
  }
}, {
  heading: 'Основы ГЧП',
  img: {
    src: '__static__img/content/products/3.jpg'
  },
  properties: {
    date: '21-23 апреля'
  },
  tags: [{
    title: 'Специалист'
  }],
  button: {
    href: '#',
    title: 'Узнать больше',
    type: 'is-white',
    size: 'is-sm'
  }
}]};
