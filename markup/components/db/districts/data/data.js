var data = {districts: [
    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Приволжский федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Центральный федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Южный федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Дальневосточный федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Сибирский федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Северо-Западный федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Северо-Кавказский федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }]
    },

    {
      href: '#',
      icon: '__static__img/content/districts/privolzhskiy.svg',
      title: 'Уральский федеральный округ',
      count: '17 публичных партнёров',
      regions: [{
        href: '#',
        title: 'Республика Башкортостан',
        icon: '__static__img/content/regions/1.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Татарстан',
        icon: '__static__img/content/regions/2.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Республика Марий Эл',
        icon: '__static__img/content/regions/3.svg',
        competition: 24,
        realize: 24,
        planned: 19,
        objects: 34,
        municipalities: [{
          href: '#',
          title: 'Агрызский муниципальный район',
          center: 'г. Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаевский муниципальный район',
          center: 'г. Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Аксубаевский муниципальный район',
          center: 'пгт. Аксубаево',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Актанышский муниципальный район',
          center: 'с. Актаныш',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Алексеевский муниципальный район',
          center: 'с. Актаныш',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }],
        cities: [{
          href: '#',
          title: 'Агрыз',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Азнакаево',
          competition: 1,
          realize: 1,
          planned: 0,
          objects: 5
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }, {
          href: '#',
          title: 'Альметьевск',
          competition: 0,
          realize: 0,
          planned: 0,
          objects: 0
        }, {
          href: '#',
          title: 'Бавлы',
          competition: 4,
          realize: 4,
          planned: 6,
          objects: 0
        }, {
          href: '#',
          title: 'Болгары',
          competition: 2,
          realize: 2,
          planned: 2,
          objects: 2
        }]
      }, {
        href: '#',
        title: 'Агрызский муниципальный район',
        icon: '__static__img/content/federals/1.svg',
        competition: 5,
        realize: 0,
        planned: 1,
        objects: 2,
        isMunicipal: true
      }, {
        href: '#',
        title: 'Алексеевский муниципальный район',
        icon: '__static__img/content/federals/1.svg',
        competition: 5,
        realize: 0,
        planned: 1,
        objects: 2,
        isMunicipal: true
      }]
    }
]};
