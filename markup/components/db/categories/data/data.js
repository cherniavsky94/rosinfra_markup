var data = {categories: {
  search: [{
    value: {
      title: '5 689'
    },
    caption: 'Реализуемых проектов',
    img: {
      src: '__static__img/content/categories/projects.svg',
      hover: '__static__img/content/categories/projects-white.svg'
    }
  }, {
    value: {
      title: '3 543'
    },
    caption: 'Отраслевых экспертов',
    img: {
      src: '__static__img/content/categories/group.svg',
      hover: '__static__img/content/categories/group-white.svg'
    }
  }, {
    value: {
      title: '1 436'
    },
    caption: 'Проектных инициатив',
    img: {
      src: '__static__img/content/categories/initiatives.svg',
      hover: '__static__img/content/categories/initiatives-white.svg'
    }
  }, {
    value: {
      title: '10 355'
    },
    caption: 'Участников рынка',
    img: {
      src: '__static__img/content/categories/partners.svg',
      hover: '__static__img/content/categories/partners-white.svg'
    }
  }],

  country: [{
    value: {
      title: '136'
    },
    caption: 'Реализуемые проекты',
    img: {
      src: '__static__img/content/stages/implemented.svg'
    },
    size: 'is-stage'
  }, {
    value: {
      title: '12'
    },
    caption: 'Проекты на конкурсе',
    img: {
      src: '__static__img/content/stages/competition.svg'
    },
    size: 'is-stage'
  }, {
    value: {
      title: '2'
    },
    caption: 'Планируемые проекты',
    img: {
      src: '__static__img/content/stages/planned.svg'
    },
    size: 'is-stage'
  }, {
    value: {
      title: '136'
    },
    caption: 'Объекты под проекты ГЧП',
    img: {
      src: '__static__img/content/stages/objects.svg'
    },
    size: 'is-stage'
  }],

  advantages: [{
    type: 'is-advantage',
    title: 'Гибкий график обучения',
    caption: 'Программы обучения продолжительностью от нескольких часов до 1 месяца',
    img: {
      src: '__static__img/content/categories/time.svg',
      // hover: '__static__img/content/categories/projects-white.svg'
    }
  }, {
    type: 'is-advantage',
    title: 'Разноуровневые программы',
    caption: 'Возможность освоить тему ГЧП с нуля или в разрезе недостающих компетенций',
    img: {
      src: '__static__img/content/categories/programs.svg',
      // hover: '__static__img/content/categories/initiatives-white.svg'
    }
  }, {
    type: 'is-advantage',
    title: 'Персональный подход',
    caption: 'Выстраивание индивидуальной траектории обучения',
    img: {
      src: '__static__img/content/categories/search.svg',
      // hover: '__static__img/content/categories/investments-white.svg'
    }
  }, {
    type: 'is-advantage',
    title: 'Квалифицированные преподаватели',
    caption: 'Ведущие эксперты, обладающие теоретическими знаниями и практическом опытом реализации проектов\n',
    img: {
      src: '__static__img/content/categories/group.svg',
      // hover: '__static__img/content/categories/partners-white.svg'
    }
  }],

  experts: [{
    type: 'is-expert',
    title: 'Широкий выбор экспертов',
    caption: 'Самая большая база экспертов в сфере ГЧП',
    img: {
      src: '__static__img/content/categories/select.svg'
    }
  }, {
    type: 'is-expert',
    title: 'Сокращение издержек',
    caption: 'Экономия времени на логистику и проведение встреч',
    img: {
      src: '__static__img/content/categories/time.svg'
    }
  }, {
    type: 'is-expert',
    title: 'Контроль качества',
    caption: 'Гарантия высокого качества оказанных услуг',
    img: {
      src: '__static__img/content/categories/quality.svg'
    }
  }, {
    type: 'is-expert',
    title: 'Гибкое ценообразование',
    caption: 'Стоимость услуг зависит от объема работ',
    img: {
      src: '__static__img/content/categories/price.svg'
    }
  }],

  steps: [{
    type: 'is-step',
    number: 1,
    title: 'Перейдите к заполнению анкеты эксперта',
    caption: 'Для этого подайте заявку в личном кабинете (перейти в личный кабинет)',
    img: {
      src: '__static__img/content/categories/anketa.svg'
    }
  }, {
    type: 'is-step',
    number: 2,
    title: 'Заполните анкету, подробно описав свой опыт работы',
    caption: 'Подробно заполните все поля анкеты и укажите свои контактные данные',
    img: {
      src: '__static__img/content/categories/projects.svg'
    }
  }, {
    type: 'is-step',
    number: 3,
    title: 'Отправка анкеты происходит в режиме онлайн',
    caption: 'После рассмотрения вам будет присвоен статус "Эксперт"',
    img: {
      src: '__static__img/content/categories/send.svg'
    }
  }]
}};
