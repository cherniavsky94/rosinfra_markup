var data = {sectionTariffs: {
  business: [
    {
      title: 'Базовый доступ',
      type: 'is-free',
      props: [
        {
          label: 'Количество пользователей',
          value: '-'
        },
        {
          label: 'База проектов',
          value: 'Ограниченный доступ',
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          value: 'Ограниченный доступ',
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '3 часа',
          type: 'is-primary'
        },
        {
          label: 'Продвижение компании и ее проектов',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Позиционирование на ключевых мероприятиях',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '0'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          price: {
            value: '0'
          },
          type: 'is-primary'
        }
      ],
      buttons: [
        {
          title: 'Оформить подписку',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'subscribe-form'
        }, {
          title: 'Проконсультироваться',
          type: 'is-secondary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    },

    {
      title: 'Базовый',
      props: [
        {
          label: 'Количество пользователей',
          count: 3
        },
        {
          label: 'База проектов',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '2 ч./ квартал',
          type: 'is-primary'
        },
        {
          label: 'Продвижение компании и ее проектов',
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Позиционирование на ключевых мероприятиях',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '33 000',
            label: '(при оплате за квартал)'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          price: {
            value: '26 500',
            label: '(при оплате за год)',
            badge: {
              type: 'is-danger',
              title: 'Выгода до -20%'
            }
          },
          type: 'is-primary'
        }
      ],
      buttons: [
        {
          title: 'Оформить подписку',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'subscribe-form'
        }, {
          title: 'Проконсультироваться',
          type: 'is-secondary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    },

    {
      title: 'Расширенный',
      props: [
        {
          label: 'Количество пользователей',
          count: 3
        },
        {
          label: 'База проектов',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '3 ч./ квартал',
          type: 'is-primary'
        },
        {
          label: 'Продвижение компании и ее проектов',
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Позиционирование на ключевых мероприятиях',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '66 000',
            label: '(при оплате за квартал)'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          price: {
            value: '54 000',
            label: '(при оплате за год)',
            badge: {
              type: 'is-danger',
              title: 'Выгода до -20%'
            }
          },
          type: 'is-primary'
        }
      ],
      buttons: [
        {
          title: 'Оформить подписку',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'subscribe-form'
        }, {
          title: 'Проконсультироваться',
          type: 'is-secondary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    },

    {
      title: 'Акселератор',
      type: 'is-danger',
      props: [
        {
          label: 'Количество пользователей',
          count: 5
        },
        {
          label: 'База проектов',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '5 ч./ квартал',
          type: 'is-primary'
        },
        {
          label: 'Продвижение компании и ее проектов',
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Позиционирование на ключевых мероприятиях',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта'
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '-',
            label: '(при оплате за квартал)',
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          type: 'is-primary',
          price: {
            value: '1 299 000'
          },
        }
      ],
      buttons: [
        {
          title: 'Проконсультироваться',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    }
  ],

  government: [
    {
      title: 'Бесплатно',
      type: 'is-free',
      props: [
        {
          label: 'Количество пользователей',
          value: '-'
        },
        {
          label: 'Персональный профиль',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'База проектов',
          value: 'Ограниченный доступ',
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          value: 'Ограниченный доступ',
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '3 часа',
          type: 'is-primary'
        },
        {
          label: 'Продвижение проектов',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Презентация проекта (роуд-шоу)',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Сбор данных для расчета показетеля "Уровень развития ГЧП"',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Проведение обучения для проектных команд',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '0'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          price: {
            value: '0'
          },
          type: 'is-primary'
        }
      ],
      buttons: [
        {
          title: 'Оформить подписку',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'subscribe-form'
        }, {
          title: 'Проконсультироваться',
          type: 'is-secondary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    },

    {
      title: 'Базовый',
      props: [
        {
          label: 'Количество пользователей',
          count: 3
        },
        {
          label: 'Персональный профиль',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'База проектов',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '3 ч./ квартал',
          type: 'is-primary'
        },
        {
          label: 'Продвижение проектов',
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Презентация проекта (роуд-шоу)',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Сбор данных для расчета показетеля "Уровень развития ГЧП"',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта',
          img: {
            src: '__static__img/general/close.svg'
          }
        },
        {
          label: 'Проведение обучения для проектных команд',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '33 000',
            label: '(при оплате за квартал)'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          price: {
            value: '25 000',
            label: '(при оплате за год)',
            badge: {
              type: 'is-danger',
              title: 'Выгода до -25%'
            }
          },
          type: 'is-primary'
        }
      ],
      buttons: [
        {
          title: 'Оформить подписку',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'subscribe-form'
        }, {
          title: 'Проконсультироваться',
          type: 'is-secondary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    },

    {
      title: 'Расширенный',
      props: [
        {
          label: 'Количество пользователей',
          count: 5
        },
        {
          label: 'Персональный профиль',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'База проектов',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '5 ч./ квартал',
          type: 'is-primary'
        },
        {
          label: 'Продвижение проектов',
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Презентация проекта (роуд-шоу)',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Сбор данных для расчета показетеля "Уровень развития ГЧП"',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Проведение обучения для проектных команд',
          img: {
            src: '__static__img/general/close.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            value: '66 000',
            label: '(при оплате за квартал)'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          price: {
            value: '50 000',
            label: '(при оплате за год)',
            badge: {
              type: 'is-danger',
              title: 'Выгода до -25%'
            }
          },
          type: 'is-primary'
        }
      ],
      buttons: [
        {
          title: 'Оформить подписку',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'subscribe-form'
        }, {
          title: 'Проконсультироваться',
          type: 'is-secondary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    },

    {
      title: 'Акселератор',
      type: 'is-danger',
      props: [
        {
          label: 'Количество пользователей',
          count: 10
        },
        {
          label: 'Персональный профиль',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'База проектов',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Отраслевая аналитика и ежемесячный дайджест',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Онлайн-консультации с квалифицированными экспертами',
          value: '10 ч./ квартал',
          type: 'is-primary'
        },
        {
          label: 'Продвижение проектов',
          type: 'is-primary'
        },
        {
          label: 'Блиц-экспертиза проекта',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Поиск партнеров и организация переговоров',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Презентация проекта (роуд-шоу)',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Индивидуальное сопровождение',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Сопровождение работы с платформой',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Экспертная поддержка 24/7',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Сбор данных для расчета показетеля "Уровень развития ГЧП"',
          img: {
            src: '__static__img/general/check.svg'
          }
        },
        {
          label: 'Кейс-анализ и экспертная оценка проекта'
        },
        {
          label: 'Проведение обучения для проектных команд',
          img: {
            src: '__static__img/general/check.svg'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена квартальной подписки:',
          price: {
            label: 'Сумма зависит от количества участников образовательной программы'
          },
          type: 'is-primary'
        },
        {
          label: 'Цена годовой подписки (действительна при условии 100% аванса):',
          type: 'is-primary',
          price: {
            label: 'Свяжитесь с нами и мы подберем для вас лучшие условия',
            isAccent: true
          }
        }
      ],
      buttons: [
        {
          title: 'Проконсультироваться',
          type: 'is-primary is-round',
          size: 'is-average',
          modal: 'get-consult'
        }
      ]
    }
  ]
}};
