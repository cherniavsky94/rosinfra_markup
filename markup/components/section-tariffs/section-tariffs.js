export default {
  init() {
    $(window).on('load resize', this.height);

    if ($(window).width() < 1024) {
      new Swiper('.section-tariffs__inner', {
        slidesPerView: 1.1,
        wrapperClass: 'section-tariffs__grid',
        slideClass: 'section-tariffs__column',
        breakpointInverse: true,
        breakpoints: {
          640: {
            slidesPerView: 1.5
          }
        }
      });
    }
  },

  height() {
    const $columns = $('.js-tarrif-column'),
       rowHeights = [],
       highestHeight = [];

    $columns.each(function (columnID) {
      const $column = $(this);

      $column.children().each(function (rowID) {
        if (columnID === 0) {
          let rowHeight = $(this).outerHeight();
          rowHeights.push([rowHeight]);
        } else {
          let rowHeight = $(this).outerHeight();
          rowHeights[rowID].push(rowHeight);
        }
      });
    });

    for (let i = 0; i < rowHeights.length; i++) {
      let height = Math.max.apply(null, rowHeights[i]);
      highestHeight.push(height);
    }

    $columns.each(function () {
      const $column = $(this);

      $column.children().each(function (rowID) {
        $(this).outerHeight(highestHeight[rowID]);
      });
    });
  }
}
