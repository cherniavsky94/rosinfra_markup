'use strict';

// const $html = $('html');
// const _window = $(window);
// const _document = $(document);
const easingSwing = [0.02, 0.01, 0.47, 1]; // default jQuery easing

var APP = window.APP || {};

APP.Dev = APP.Dev || {};
APP.Browser = APP.Browser || {};
APP.Plugins = APP.Plugins || {};
APP.Modules = APP.Modules || {};
APP.Components = APP.Components || {};
APP.Components.Digest = APP.Components.Digest || {};
APP.Modals = APP.Modals || {};

(function($, APP) {
  APP.Initilizer = function() {
    let app = {};

    app.init = function() {
      app.initGlobalPlugins();
      app.initPlugins();
      app.initComponents();
    };

    app.onLoadTrigger = function() {
      // APP.Plugins.Preloader.loaded();
      // APP.Plugins.LazyLoadImages.init();
    };

    app.refresh = function() {
      app.initPlugins(true);
      app.initComponents(true);
    };

    app.destroy = function() {};

    // pjax triggers
    app.newPageReady = function() {
      app.refresh();
    };

    app.transitionCompleted = function() {

    };

    // Global plugins which must be initialized once
    app.initGlobalPlugins = function() {
      // APP.Plugins.Barba.init();
    };

    // Plugins which depends on DOM and page content
    app.initPlugins = function(fromPjax) {

    };

    app.initComponents = function(fromPjax) {
      APP.Components.PageHeader.init();
      APP.Components.PageSearch.init();
      APP.Components.InputText.init();
      APP.Components.SectionSlider.init();
      APP.Components.SectionTariffs.init();
      APP.Components.CategoriesSlider.init();
      APP.Components.AdvantsSlider.init();
      APP.Components.Menu.init();
      APP.Components.MenuSections.init();
      APP.Components.Select.init();
      APP.Components.Accordion.init();
      APP.Components.List.init();
      APP.Components.TableList.init();

      $('[data-remodal-id]').remodal({
        hashTracking: false
      })

      // APP.Modals.Auth = $('[data-remodal-id="auth"]').remodal();
      // APP.Modals.Register = $('[data-remodal-id="register"]').remodal();

      if ($('.js-digest-rating-chart').length) {
        APP.Components.Digest.Rating.Chart.init('/rating2019.csv');
      }

      if ($('.js-digest-rating-list').length) {
        APP.Components.Digest.Rating.List.init('/rating2019.csv');
      }

      if ($('.js-digest-rating-chart-old').length) {
        APP.Components.Digest.RatingOld.Chart.init('/rating2018.csv');
      }

      if ($('.js-digest-rating-list-old').length) {
        APP.Components.Digest.RatingOld.List.init('/rating2018.csv');
      }

      $(".js_scroll_auto").click(function (){
        $('html, body').animate({
          scrollTop: $($(this).attr('data-scroll-auto')).offset().top - 100
        }, 800);
      });

      $(window).on('load', function() {
        if ($(window).width() >= 1024){
          $('.mm-menu').remove();
          $('.mm-wrapper__blocker').remove();
          $('.page').addClass('mm-wrapper_opening');
          $('.page__wrapper').removeClass('.mm-page .mm-slideout');
        }
      });
      
      $('.js-scrollbar').scrollbar();

      $('.js-mobile-filter-btn').on('click', function(){
        $('.js-new-mobile-filter').toggleClass('is-active');
        $(this).toggleClass('is-active');
        $('.js-main-aside-sticky').removeClass('is_stuck');
        $('.js-main-aside-sticky').css('position', 'static');
        $('.js-main-aside-sticky').css('width', '100%');
        return false;
      });

      $('.js-timer').each(function(){
        let $this = $(this),
            $attr = $this.attr('data-timer'),
            $card = $this.closest('.card-video'),
            $stat = $card.find('.js-video-status'),
            timezoneOffset = +$this.data('timer-offset') || 0;
            
        $this.downCount({
          date: $attr,
          offset: timezoneOffset
        }, function () {
          $this.hide();
          $this.addClass('is-hidden');
          $stat.html('Идёт в реальном времени');
        });
      });

      $('.chat').each(function(){
        var $this = $(this),
            $count = $this.find('.js-chat-value'),
            $form = $this.find('.chat__scroll');
        
        $form.on('scroll', function() {
          var $msg = $this.find('.msg').length;
          $count.html(`(` + $msg  + `)`);
        });
      });

      $('.prof').each(function () {
        var $this = $(this),
            $list = $(this).find('.prof__body');
        $this.on('click', function () {
          $list.toggleClass('is-visible');
        });
        $(document).mouseup(function (e) {
          var div = $('.prof');

          if (!div.is(e.target) && div.has(e.target).length === 0) {
            $('.prof__body').removeClass('is-visible');
          }
        });
      });

      $('.chat__input--user-name').on('keydown', function( e ) {
        if( e.keyCode === 13 ) {
          e.preventDefault();
          $('.chat__input--user-message').focus();
        }
      });

      $('.js-chat-checkbox').styler();

      $('.select').each(function(){
        var $this = $(this),
            $removeBtn = $this.find('.js-clear-select'),
            $select = $this.find('.js-select2');

        $select.on('change', function(){
          
          if($(this).val()){
            $removeBtn.css('display','block');
          } else {
            $removeBtn.css('display','none');
          }
        });

        $removeBtn.on('click', function(){
          $select.val(null).trigger('change');
          $removeBtn.css('display','none');
        });
      });

      $('textarea').on('keydown', function( e ) {
        if( e.keyCode === 13 ) {
          return false;
          whenEnterPressed();
        }
      });

      $('.menu-video__item').on('click', function () {
        $(this).addClass('is-current').siblings().removeClass('is-current');
    
        $('.js-video-tab').eq($(this).index()).addClass('is-current').siblings().removeClass('is-current');
      });

      $(document).on('click', '.program-card__button', function(event) {
        var url = $(this).data('url');

        if (url) {
          event.preventDefault();
          window.location.href = url;
        }
      });

      $(document).on('mouseenter', '.js-charters-aside', function() {
        var $block = $(this),
            $arrow = $block.find('.js-charters-count'),
            $drop = $block.find('.js-charters-drop');

        $arrow.addClass('active');
        $drop.addClass('visible');
      });

      $(document).on('mouseleave', '.js-charters-aside', function() {
        var $block = $(this),
            $arrow = $block.find('.js-charters-count'),
            $drop = $block.find('.js-charters-drop');

        $arrow.removeClass('active');
        $drop.removeClass('visible');
      });

      $('.js-section-ajax').each(function() {
        var $section = $(this),
            $slider = $section.find('.js-section-slider-ajax'),
            $wrapper = $slider.find('.js-slider-wrapper'),
            $link = $section.find('.js-menu-ajax');

        var slider = ajaxSliderInit($section);

        $link.parent().eq(0).addClass('is-active');

        $link.on('click', function(event) {
          event.preventDefault();

          $slider.addClass('is-loading');

          var $el = $(this),
              href = $el.attr('href');

          if (href !== '#') {
            axios.get(href).then(function (response) {
              slider.removeAllSlides();

              var $slides = $(response.data).filter('.js-slider-slide');
              var slides = [];

              if ($(response.data).hasClass('js-slider-wrapper')) {
                $slides = $(response.data).children().filter('.js-slider-slide');
              }

              for (var i = 0; i < $slides.length; i++) {
                slides.push($slides[i]);
              }

              slider.appendSlide(slides);
            }).finally(function(){
              setTimeout(function(){
                $slider.removeClass('is-loading');
                slider.update();
                slider.navigation.update();
                slider.pagination.update();

                console.log(slider)
              }, 300);
            });

            $el.parent().addClass('is-active').siblings().removeClass('is-active');
          }
        });
      });

      function ajaxSliderInit($section) {
        var $slider = $section.find('.js-section-slider-ajax');
        var $prevEl = $section.find('.js-slider-prev');
        var $nextEl = $section.find('.js-slider-next');
        var $counter = $section.find('.js-slider-counter');
        var $pagination = $section.find('.js-slider-pagination');

        var slider = new Swiper($slider.get(0), {
          slidesPerView: 1.15,
          simulateTouch: false,
          wrapperClass: 'js-slider-wrapper',
          slideClass: 'js-slider-slide',
          breakpointInverse: true,
          navigation: {
            prevEl: $prevEl.get(0),
            nextEl: $nextEl.get(0)
          },
          pagination: {
            el: $pagination.get(0),
            clickable: true
          },
          breakpoints: {
            640: {
              slidesPerView: 2,
              slidesPerGroup: 2,
              pagination: {
                el: $counter.get(0),
                type: 'fraction',
                formatFractionCurrent(number) {
                  return (number < 10) ? '0' + number : number;
                },
                formatFractionTotal(number) {
                  return (number < 10) ? '0' + number : number;
                }
              },
            },

            1024: {
              slidesPerView: 3,
              slidesPerGroup: 3,
              pagination: {
                el: $counter.get(0),
                type: 'fraction',
                formatFractionCurrent(number) {
                  return (number < 10) ? '0' + number : number;
                },
                formatFractionTotal(number) {
                  return (number < 10) ? '0' + number : number;
                }
              }
            },

            1260:{
              slidesPerView: $slider.data('slides') || 3,
              slidesPerGroup: $slider.data('slides') || 3,
              pagination: {
                el: $counter.get(0),
                type: 'fraction',
                formatFractionCurrent(number) {
                  return (number < 10) ? '0' + number : number;
                },
                formatFractionTotal(number) {
                  return (number < 10) ? '0' + number : number;
                }
              },
            }
          }
        });

        // slider.on('transitionStart', function() {
        //   console.log('transitionStart')
        //   $pagination.children().eq(this.activeIndex).addClass('swiper-pagination-bullet-active').siblings().removeClass('swiper-pagination-bullet-active');
        // });

        return slider;
      }

      new Swiper('.js-informer-slider', {
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        loop: true,
        slidesPerView: 1,
        spaceBetween: 10,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        navigation: {
          prevEl: '.js-slider-prev',
          nextEl: '.js-slider-next'
        },
        pagination: {
          el: '.js-slider-pagination',
          clickable: true
        },
        on: {
          init() {
            this.el.addEventListener('mouseenter', () => {

            this.autoplay.stop();
              });
             this.el.addEventListener('mouseleave', () => {
               this.autoplay.start();
             });
           }
         },
      });

      $(document).on('click', '.program-card', function(event) {
        if ($(window).width() <= 1024) {
          event.preventDefault();

          var url = $(this).attr('href');

          if (url) {
            window.location.href = url;
          } else {
            url = $(this).find('.program-card__more').attr('href');
          }
        }
      });

      var galleryThumbs = new Swiper('.js-materials-thumbs', {
        spaceBetween: 15,
        slidesPerView: 7,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,

      });

      var galleryTop = new Swiper('.js-materials-slider', {
        slidesPerView: 1.1,
        spaceBetween: 10,
        navigation: {
          nextEl: '.js-slider-next',
          prevEl: '.js-slider-prev',
        },
        thumbs: {
          swiper: galleryThumbs,
        },
        breakpoints: {
          767: {
            slidesPerView: 1
          }
        }
      });

      $(document).on('click', '.program-card__button', function(event) {
        var url = $(this).data('url');

        if (url) {
          event.preventDefault();
          window.location.href = url;
        }
      });

      $(document).on('click', '.program-card', function(event) {
        if ($(window).width() <= 1024) {
          event.preventDefault();

          var url = $(this).attr('href');

          if (url) {
            window.location.href = url;
          } else {
            url = $(this).find('.program-card__more').attr('href');

            if (url) {
              window.location.href = url;
            }
          }
        }
      });

      $('.card-preview--lessons').each(function() {
        var $dropdown = $(this).find('.js-accordion-body');

        if ($dropdown.length) {
          $(this).addClass('is-dropdown');
        }
      });

      $('.js-mobile-filter-btn').on('click', function(){
        $('.js-new-mobile-filter').toggleClass('is-active');
        $(this).toggleClass('is-active');
        $('.js-main-aside-sticky').removeClass('is_stuck');
        $('.js-main-aside-sticky').css('position', 'static');
        $('.js-main-aside-sticky').css('width', '100%');
        return false;
      });

      $('.widget-form__group').each(function(){
        let $this = $(this),
            $input = $('.js-custom-select');

        $input.on('click', function () {
          $input.not(this).removeClass('is-opened');
        });

      });

      $(window).scroll(function() {
        var targetLibs = $('.section-default--libs');

        if (targetLibs.length){
          var target = $('.section-default--libs').offset().top;

          if (jQuery(this).scrollTop() > target) {
            $('.libs-detail__actions-inner').addClass('is-hidden');
          } else{
            $('.libs-detail__actions-inner').removeClass('is-hidden');
          } 
          if(jQuery(window).scrollTop()+jQuery(window).height()>=jQuery(document).height()){
            $('.libs-detail__actions-inner').addClass('is-hidden');
          }   
        }
      }); 

      $('.language-switcher').each(function() {
        let $this = $(this),
            $link = $(this).find('.language-switcher__link.is-current'),
            $list = $(this).find('.language-switcher__list');

        $this.on('click', function(){
          $link.toggleClass('is-active');
          $list.toggleClass('is-visible');
        });

        $(document).mouseup(function (e){
          let div = $('.language-switcher');

          if (!div.is(e.target)
              && div.has(e.target).length === 0) { 
            $('.language-switcher__link').removeClass('is-active');
            $('.language-switcher__list').removeClass('is-visible');
          }
        });
      });

      $('.js-filter-catalog').each(function() {
        var $filter = $(this),
            $count = $filter.find('.js-filter-catalog-count'),
            $reset = $('.js-filter-catalog-reset'),
            $checkboxes = $filter.find('input[type=checkbox]'),
            $ranges = $filter.find('.js-range-group-slider'),
            $countMobile = $('.js-filter-catalog-count-mobile');

        var $selected = $checkboxes.filter(function() {
          return $(this).prop('checked');
        });

        if (window.location.search && window.location.search.length) {
          var params = window.location.search.split('&');

          if (params.length) {
            $count.css('display', 'inline-block').html($selected.length);
            $countMobile.css('display', 'inline-flex').html($selected.length);
          } else {
            $reset.hide();
            
          }

          if ( $selected.length) {
            $reset.show();
          } else {
            $count.hide();
            $countMobile.hide();
          }
          
        } else {
          $reset.hide();
          $count.hide();
          $countMobile.hide();
        }
      });

      // $('.widget-form').on('change', function() {
      //    axios.get('/digest/documents/search').then(function(response){
      //     var count = response.data.totalCount,
      //         btn = $('.button__mobile-button-value');

      //     btn.html('('+count+')');
      //   })
      // })

      $('.js-new-open-region').on('click', function(){

        $(this).toggleClass('is-cur');

        if ($(this).hasClass('is-cur')){
          $(this).html('Скрыть список');
        } else {
          $(this).html('Раскрыть список');
        }
      });

      $('.js-accordion-toggle').on('click', function(){
        $(this).toggleClass('is-cur');
      });

      $(document).on('click', '.js-password-toggle', function() {
        const $toggle = $(this),
          $container = $toggle.closest('.js-password'),
          $input = $container.find('.js-password-input');

        if ($input.attr('type') === 'password') {
          $toggle.html('<i class="fe fe-eye"></i>');
          $input.attr('type', 'text');
        } else {
          $toggle.html('<i class="fe fe-eye-off"></i>');
          $input.attr('type', 'password');
        }
      });

      $('.js-range-group').each(function () {
        var $group = $(this),
            $slider = $group.find('.js-range-group-slider'),
            $min = $group.find('.js-range-group-min'),
            $max = $group.find('.js-range-group-max'),
            $minHidden = $group.find('.js-range-group-min-hidden'),
            $maxHidden = $group.find('.js-range-group-max-hidden'),
            data = $slider.data();

        var instance = noUiSlider.create($slider.get(0), {
          start: [data.start, data.end],
          step: data.step,
          connect: true,
          range: {
            min: data.min,
            max: data.max
          }
        });

        instance.on('update', function (values) {
          $min.val(parseInt(values[0]));
          $max.val(parseInt(values[1]));

          $minHidden.val(parseInt(values[0]));
          $maxHidden.val(parseInt(values[1]));
        });

        instance.on('change', function () {
          $min.trigger('change');
          $min.trigger('change');
        });

        $min.inputmask({
          regex: $min.data('regex')
        }).on('input', function() {
          instance.set([parseInt($min.inputmask('unmaskedvalue')), null]);
        });

        $max.inputmask({
          regex: $max.data('regex')
        }).on('input', function() {
          instance.set([null, parseInt($max.inputmask('unmaskedvalue'))]);
        });

        instance.on('slide', function (values) {
          $min.val(parseInt(values[0]));
          $max.val(parseInt(values[1]));
        });
      });

      $('.charters').each(function(){
        var $this = $(this),
            $aside = $this.find('.js-charters-aside'),
            $count = $this.find('.js-charters-count'),
            $drop = $this.find('.js-charters-drop'),
            $dropitem = $this.find('.js-charters-dropitem').length;

        if ($dropitem >= 1) {
          $count.html($dropitem);
        } else {
          $aside.css('display' , 'none');
        }

        $aside.on('click', function(){
          $count.toggleClass('active');
          $drop.toggleClass('visible');
        });

        // $(document).mouseup(function (e){
        //   if (!$drop.is(e.target) 
        //       && $drop.has(e.target).length === 0) {
        //     $count.removeClass('active');
        //     $drop.removeClass('visible');
        //   }
        // });

      });

      $('.calendar--new').each(function(){
        var $this = $(this),
            $input = $this.find('.calendar__input'),
            $calendar = $this.find('.flatpickr-calendar'),
            $close = $this.find('.calendar__close-btn');

        $input.on('click', function(){
          $this.toggleClass('opened');
        });

        $close.on('click', function(){
          $this.removeClass('opened');
        });

        $(document).click( function(event){
          if( $(event.target).closest('.flatpickr-calendar, .calendar--new').length )
            return;
          $this.removeClass('opened');
        });

      });

      $('.js-profile-card').each(function(){
        var $this = $(this),
            $header = $this.find('.profile-card__header'),
            $body = $this.find('.profile-card__body');

        $header.on('click', function(){
          $(this).toggleClass('opened');
          $body.toggleClass('opened');
        })
      });

      $('.tag__body').each(function(){
        var $this = $(this),
            $list = $this.find('.tag__list'),
            $btn = $this.find('.tag__show-all'),
            $height = $this.find(".tag__list").height();

        if ($height > 20){
          $($list).addClass('height');
        } else  {
          if ($height <= 20){
            $btn.css('display', 'none');
          }
        }

        $btn.on('click', function(){
          $(this).toggleClass('is-active');
          $list.toggleClass('is-active');
          return false;
        })
      });

      $('.comments__content').each(function(){
        let $this = $(this),
            $card = $this.find('.comments__section'),
            $qn  = $this.find('.comments__section').length,
            $btn = $this.children('.comments__more');

        if ($qn > 2){
            $btn.css('display','flex');
        } else {
            $btn.css('display','none');
        }

      });

      var $btn = $('.comments__more');

      $btn.on('click' , function(){
        $btn = $(this);
        var $card = $btn.closest('.comments__content').children().children('.comments__section');
        $(this).toggleClass('active');
        $card.toggleClass('active');

        if ($card.hasClass('active')){
            $(this).html('Скрыть комментарии');
        } else {
            $(this).html('Показать ещё комментарии');
        }

        return false;
      });


      $('.comments-card').each(function(){
        var $card = $(this),
            $text = $card.find('.comments-card__text'),
            $height = $card.find('.comments-card__text').height(),
            $button = $card.find('.comments-card__button--accent');

        if ($height > 40){
          $text.addClass('height');
        } else  {
          if ($height <= 40){
            $button.css('display', 'none');
          }
        }

        $button.on('click' , function(){
          $text.toggleClass('height');
          $(this).toggleClass('active');

          if ($(this).hasClass('active')){
              $(this).html('Скрыть полный комментарий');
          } else {
              $(this).html('Раскрыть полный комментарий');
          }

          return false;
        });
      });

      $('.main-nav__list').each(function () {
        var $list = $(this),
            $link = $list.find('.main-nav__link.is-title');

        $link.on('click', function(event){
          if ($(window).width() < 768 ){
            event.preventDefault();
            $list.toggleClass('is-opened');
            $link.toggleClass('is-opened');
          }
        });
      });


      $('.js-card-preview-item').on('click', function(){
        $(this).children('.card-preview__link-video').slideToggle();
        $(this).toggleClass('opened');
      });

      $(window).scroll(function() {
        var targetLibs = $('.section-default--libs');

        if (targetLibs.length){
          var target = $('.section-default--libs').offset().top;

          if (jQuery(this).scrollTop() > target) {
            $('.libs-detail__actions-inner').addClass('is-hidden');
          } else{
            $('.libs-detail__actions-inner').removeClass('is-hidden');
          } 
          if(jQuery(window).scrollTop()+jQuery(window).height()>=jQuery(document).height()){
            $('.libs-detail__actions-inner').addClass('is-hidden');
          }   
        }
      }); 

      var swiper = new Swiper('.js-profile-swiper', {
        slidesPerView: 1,
        pagination: {
          el: '.profile-banners__pagination',
          type: 'fraction',
        },
        navigation: {
          nextEl: '.profile-banners__swiper-btn--next',
          prevEl: '.profile-banners__swiper-btn--prev',
        },
      });

      // const $rangeSlider = $('.js-search-range-slider');
      //
      // const slider = noUiSlider.create($rangeSlider.get(0), {
      //   start: [0, 500000000],
      //   step: 10000,
      //   connect: true,
      //   range: {
      //     min: 0,
      //     max: 1000000000
      //   }
      // });
      //
      // slider.on('update', function (values) {
      //   $('.js-range-slider-min').val(parseInt(values[0]).toLocaleString());
      //   $('.js-range-slider-max').val(parseInt(values[1]).toLocaleString());
      // });

      $(".js-select-date").flatpickr();
      // $('.js-select-time').timepicker({ 'timeFormat': 'H:i' });

      new Swiper('.js-section-stats-slider', {
        slidesPerView: 1,
        wrapperClass: 'js-slider-wrapper',
        slideClass: 'js-slider-slide',
        navigation: {
          prevEl: '.js-section-stats-prev',
          nextEl: '.js-section-stats-next'
        }
      });


      // Mmenu.configs.offCanvas.page.selector = '.js-page-wrapper';

      var $panelSearchInput = $('.js-panel-search-input').detach();
      var $panelSearchButon = $('.js-panel-search-button').detach();

      // var menu = new Mmenu( '.js-page-aside', {
      //   navbar: {
      //     title: 'Выберите варианты'
      //   },
      //   navbars: [
      //     {
      //       position: 'top',
      //       content: [$panelSearchInput[0]]
      //     }, {
      //       position: 'bottom',
      //       content: [$panelSearchButon[0]]
      //     }
      //   ],
      //   drag: {
      //     open: true,
      //     node: document.body
      //   }
      // });

      $(document).on('click', '.js-search-mobile-toggle', function () {
        if ($('body').hasClass('is-search')) {
          $('body').removeClass('is-search is-locked');
        } else {
          menu.open();
        }
      });

      var galleryThumbs = new Swiper('.js-materials-thumbs', {
        spaceBetween: 15,
        slidesPerView: 7,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,

      });

      var galleryTop = new Swiper('.js-materials-slider', {
        slidesPerView: 1.1,
        spaceBetween: 10,
        navigation: {
          nextEl: '.js-slider-next',
          prevEl: '.js-slider-prev',
        },
        thumbs: {
          swiper: galleryThumbs,
        },
        breakpoints: {
          767: {
            slidesPerView: 1 
          }
        }
      });

      $('.js-wiget-slider').each(function() {
        var $section = $(this).closest('.js-wiget');

        new Swiper(this, {
          slidesPerView: 'auto',
          wrapperClass: 'js-slider-wrapper',
          slideClass: 'js-slider-slide',
          navigation: {
            prevEl: $section.find('.js-slider-prev'),
            nextEl: $section.find('.js-slider-next')
          },
          pagination: {
            el: '.wiget__slider-pagination',
            type: 'fraction'
          }
        });
      });

      $('.drop-list__header').on('click', function(){
        let $drop = $('.drop-list__body');

        $(this).toggleClass('opened');
        $(this).parent().find($drop).toggleClass('opened');
      });

      $('.js-scrolltop').on('click', function(event) {
        event.preventDefault();
        var section = $($(this).attr('href'));

        $('html, body').animate({scrollTop: section.offset().top});
      });

      $('.js-scroll-to').on('click', function(event) {
        event.preventDefault();

        var height_el = $('.page-header').height();
        var section = $($(this).attr('href'));

        $('html, body').animate({scrollTop: section.offset().top - height_el - 30});
      });

      $('.super-search__main').each(function(){
        var $this = $(this),
            $btn = $(this).find('.super-search__remove-value'),
            $input = $(this).find('.super-search__input');

        $btn.on('click', function(){
          $input.val('');
          window.location.reload()
        });
      });

      $('[data-scroll-target]').on('click', function(event) {
        event.preventDefault();

        var height_el = $('.page-header').outerHeight();
        var section = $('[data-scroll-id='+ $(this).data('scroll-target') +']');

        $('html, body').animate({scrollTop: section.offset().top - height_el}, 900);
      });

      $('.alphabet-list__dropdown-scrollbar, .js-area-scroll').scrollbar();

      APP.Modals.AddedToCart = $('[data-remodal-id]').remodal({
        hashTracking: false
      });
    };

    // $('.info-banner__close').on('click', function(){
    //   $('.info-banner').hide();
    //   $('.info-banner').removeClass('active');
    //   $('body').removeClass('is-banner');
    // });

    // let $banner = $('.info-banner'),
    //     $body = $('body'),
    //     $close = $('.info-banner__close'),
    //     $wrap = $('page__wrapper'),
    //     $height = $banner.outerHeight();

    // if($banner.hasClass('active')){
    //   $body.addClass('is-banner');
    // } else {
    //   $body.removeClass('is-banner');
    // }

    new Swiper('.js-head-news-slider', {
      wrapperClass: 'js-slider-wrapper',
      slideClass: 'js-slider-slide',
      breakpointsInverse: true,
      slidesPerView: 1,
      loop: true,
      autoplay: {
        delay: 9000
      },
      pagination: {
        el: '.js-article-pagination',
      },
      navigation: {
        prevEl: '.js-slider-news-prev',
        nextEl: '.js-slider-news-next'
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
        },
        640: {
          slidesPerView: 2,
        },
        768: {
          slidesPerView: 2.1,
        },

        992: {
          slidesPerView: 1,

          spaceBetween: 15,

          pagination: {

            el: '.js-article-pagination',

            type: 'fraction',

            formatFractionCurrent(number) {

              return (number < 10) ? '0' + number : number;

            },

            formatFractionTotal(number) {

              return (number < 10) ? '0' + number : number;

            }

          },

        },

        1024: {

          slidesPerView: 1,

          spaceBetween: 15,

          pagination: {

            el: '.js-article-pagination',

            type: 'fraction',

            formatFractionCurrent(number) {

              return (number < 10) ? '0' + number : number;

            },

            formatFractionTotal(number) {

              return (number < 10) ? '0' + number : number;

            }

          },

          speed: 1000,

          loop: true,

          breakpointsInverse: true,

          autoplay: {

            delay: 9000

          },

        }

      }

    });


    $(document).on('click', '.header-dropdown', function(event){
      if($(event.target).closest('.header-dropdown__inner').length) return;
      $('body').removeClass('is-menu is-blur is-locked');
    });

    $('.js-custom-select').each(function () {
      var $select = $(this),
          $header = $select.find('.js-custom-select-header'),
          $input = $select.find('.js-custom-select-input'),
          $list = $select.find('.js-select-custom-list'),
          $selected = $select.find('.js-select-cusom-selected'),
          $apply = $select.find('.js-select-custom-apply'),
          placeholder = $input.attr('placeholder'),
          options = $select.data();

      $header.on('click', function () {
        $input.attr('placeholder', options.placeholder);
        $select.addClass('is-opened');
      });

      $input.on('input', function () {
        $list.children().each(function () {
          var $item = $(this),
              title = $item.find('.input-control__label').text();

          if (title.toLowerCase().indexOf($input.val().toLowerCase()) > -1) {
            $item.show();
          } else {
            $item.hide();
          }
        });
      });

      $apply.on('click', function (event) {
        event.preventDefault();
      });

      $(document).on('click', function(event){
        if(!$(event.target).closest('.js-custom-select').length) {
          $('.js-custom-select').removeClass('is-opened');
          $input.attr('placeholder', placeholder);
        };
      });

      $selected.children().each(function () {
        var $group = $(this),
            $input = $group.find('.js-selected-custom-input'),
            $list = $group.find('.js-selected-custom-list');

        $input.on('input', function () {
          $list.children().each(function () {
            var $item = $(this),
              title = $item.find('.input-control__label').text();

            if (title.toLowerCase().indexOf($input.val().toLowerCase()) > -1) {
              $item.show();
            } else {
              $item.hide();
            }
          });
        });
      });

    });

    // $('.js-input-text-date')

    

    return app;
  };

  // a.k.a. ready
  $(function() {
    APP.Initilizer().init();
  });

  $(window).on('load', function() {
    $.ready.then(function() {
      APP.Initilizer().onLoadTrigger();
    });
  });
})(jQuery, window.APP);