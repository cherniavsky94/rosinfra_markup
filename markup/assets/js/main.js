'use strict';

import jquery from 'jquery';
import remodal from 'remodal';
import Swiper from 'swiper';
// import Mmenu from 'mmenu-js';
import noUiSlider from 'nouislider';
import stickyKit from 'sticky-kit/dist/sticky-kit';
import select2 from 'select2';
import scrollbar from 'jquery.scrollbar';
import Papa from 'papaparse';
import dropzone from 'dropzone';
import Timepicker from 'timepicker';
// import Vue from 'vue';
import flatpickr from 'flatpickr';
import { Russian } from 'flatpickr/dist/l10n/ru.js';
import fancybox from '@fancyapps/fancybox/dist/jquery.fancybox';
import Slideout from 'slideout';
import yii from './libraries/yii';
import inputmask from './plugins/inputmask';
import timer from './plugins/timer';
import Chart from 'chart.js';
import styler from './plugins/jquery.formstyler';

flatpickr.localize(Russian);

import PageHeader from './../../components/page-header/page-header';
import PageSearch from './../../components/page-search/page-search';
import InputText from './../../components/input-text/input-text';
import SectionSlider from './../../components/section-slider/section-slider';
import SectionTariffs from './../../components/section-tariffs/section-tariffs';
import SuperSearch from './../../components/super-search/super-search';
import CategoriesSlider from './../../components/categories-slider/categories-slider';
import AdvantsSlider from './../../components/advants-slider/advants-slider';
import Menu from './../../components/menu/menu';
import MenuSections from './../../components/menu-sections/menu-sections';
import Select from './../../components/select/select';
import DigestRating from './../../components/digest/rating/rating';
import DigestRatingOld from './../../components/digest/rating/rating_old';
import selectCustom from './../../components/ui/ui-select-custom/ui-select-custom';

global.jQuery = global.$ = jquery;
window.remodal = remodal;
// window.Vue = Vue;
window.stickyKit = stickyKit;
window.Swiper = Swiper;
window.select2 = select2;
window.scrollbar = scrollbar;
window.Papa = Papa;
window.noUiSlider = noUiSlider;
window.flatpickr = flatpickr;
window.dropzone = dropzone;
window.fancybox = fancybox;
window.Slideout = Slideout;
window.inputmask = inputmask;
window.styler = styler;
window.timer = timer;
window.timepicker = Timepicker;
window.Chart = Chart;
window.yiiFramework = yii;

$(function () {
  const pageHeaderHeight = $('.js-page-header').outerHeight();

  APP.Plugins.StickyKit = {
    update() {
      $(document.body).trigger("sticky_kit:recalc");
    }
  };

  APP.Plugins.ScrollTo = function ($destination) {
    const offsetTop = $destination.offset().top - pageHeaderHeight;

    $('html, body').animate({
      scrollTop: offsetTop
    }, 800);
  };

  APP.Components.PageHeader = PageHeader;
  APP.Components.PageSearch = PageSearch;
  APP.Components.InputText = InputText;
  APP.Components.SectionSlider = SectionSlider;
  APP.Components.SectionTariffs = SectionTariffs;
  APP.Components.SuperSearch = SuperSearch;
  APP.Components.CategoriesSlider = CategoriesSlider;
  APP.Components.AdvantsSlider = AdvantsSlider;
  APP.Components.Menu = Menu;
  APP.Components.MenuSections = MenuSections;
  APP.Components.Select = Select;
  APP.Components.Digest.Rating = DigestRating;
  APP.Components.Digest.RatingOld = DigestRatingOld;

  APP.Browser.Scroll = {
    locked() {
      $('body').addClass('is-locked');
    },

    unlocked() {
      $('body').removeClass('is-locked');
    }
  };


  APP.Components.Accordion = {
    options: {
      openedClassName: 'is-opened',
      closedClassName: 'is-closed',
      speed: 400
    },

    init() {
      const _this = this;

      $(document).on('click', '.js-accordion-toggle', function () {
        const $toggle = $(this),
              $item = $toggle.closest('.js-accordion-item');

        _this.isOpened($item) ? _this.close($item) : _this.open($item);
      });
    },

    open($item) {
      const $body = $item.children('.js-accordion-body');


      $body.stop().slideDown(this.options.speed, () => {
        $item.addClass(this.options.openedClassName).removeClass(this.options.closedClassName);
        APP.Plugins.StickyKit.update()
      });
    },

    close($item) {
      const $body = $item.children('.js-accordion-body');

      $body.stop().slideUp(this.options.speed, () => {
        $item.removeClass(this.options.openedClassName).addClass(this.options.closedClassName);
        APP.Plugins.StickyKit.update()
      });
    },

    isOpened($item) {
      return $item.hasClass(this.options.openedClassName);
    }
  };

  APP.Components.List = {
    init() {
      const $lists = $('.js-list');

      $lists.each((id, el) => {
        const $list = $(el),
              $items = $list.find('.js-list-item'),
              $toggle = $list.find('.js-list-toggle'),
              $toggleParent = $toggle.closest('.js-list-toggle-parent');

        const options = {
          collapsed: false,
          showCount: parseInt($list.data('show-count')) || 3,
          itemsCount: $items.length,
          toggleShowHTML: $toggle.html(),
          toggleHideHTML: '<span class="link-more-chevron box-secondary__link-more js-list-toggle">Свернуть список<i class="fe fe-chevron-up link-more-chevron__icon"></i></span>'
        };

        this.hideItems($items, $toggle, options);

        if (options.itemsCount > options.showCount) {
          if ($toggleParent.length) {
            $toggleParent.show()
          } else {
            $toggleParent.hide();
          }
        }

        $toggle.on('click', () => {
          if (!options.collapsed) {
            $items.each((id, el) => {
              const $item = $(el);
              this.showByTag($item, options, $toggle);
            });

            options.collapsed = true;
          } else {
            this.hideItems($items, $toggle, options);
            options.collapsed = false;
          }
        });
      });
    },

    showByTag($el, options, $toggle) {
      const node = $el.get(0);

      switch (node.tagName) {
        case 'TBODY':
          $el.css('display', 'table-row-group');
          break;

        default:
          $el.show();
          break;
      }

      $toggle.html(options.toggleHideHTML);
    },

    hideItems($items, $toggle, options) {
      $items.each((id, el) => {
        const $item = $(el);

        if (id < options.showCount) {
          this.showByTag($item, options, $toggle);
        } else {
          $item.hide();
        }
      });

      $toggle.html(options.toggleShowHTML);
    }
  };

  APP.Components.TableList = {
    init() {
      const $lists = $('.js-table-list-region');

      $lists.each((id, el) => {
        const $list = $(el),
              $items = $list.find('.js-table-list-region-item'),
              $toggle = $list.find('.js-table-list-region-toggle'),
              $toggleParent = $toggle.closest('.js-table-list-region-toggle-parent');

        const options = {
          collapsed: false,
          showCount: parseInt($list.data('show-count')) || 3,
          itemsCount: $items.length,
          toggleShowHTML: $toggle.html(),
          toggleHideHTML: '<span class="link-more-chevron box-secondary__link-more js-table-list-region-toggle">Свернуть список<i class="fe fe-chevron-up link-more-chevron__icon"></i></span>'
        };

        this.hideItems($items, $toggle, options);

        if (options.itemsCount > options.showCount) {
          if ($toggleParent.length) {
            $toggleParent.show()
          } else {
            $toggleParent.hide();
          }
        }

        $toggle.on('click', () => {
          if (!options.collapsed) {
            $items.each((id, el) => {
              const $item = $(el);
              this.showByTag($item, options, $toggle);
            });

            options.collapsed = true;
          } else {
            this.hideItems($items, $toggle, options);
            options.collapsed = false;
          }
        });
      });
    },

    showByTag($el, options, $toggle) {
      const node = $el.get(0);

      switch (node.tagName) {
        case 'TBODY':
          $el.css('display', 'table-row-group');
          break;

        default:
          $el.show();
          break;
      }

      $toggle.html(options.toggleHideHTML);
    },

    hideItems($items, $toggle, options) {
      $items.each((id, el) => {
        const $item = $(el);

        if (id < options.showCount) {
          this.showByTag($item, options, $toggle);
        } else {
          $item.hide();
        }
      });

      $toggle.html(options.toggleShowHTML);
    }
  };

  $(function () {
    var $container = $('.js-filter-container'),
        $source = $('.js-filter-source'),
        $aside = $('.js-filter-aside'),
        $button = $('.js-filter-button'),
        isAppended = false;

    $(window).on('load resize', function () {
      if ($(this).width() < 1024) {
        if (!isAppended) {
          $container.appendTo($aside);
          isAppended = true;
        }
      } else {
        if (isAppended) {
          $container.appendTo($source);
          isAppended = false;
        }
      }
    });

    if ($container.length && $(window).width() < 1024) {
      const menu = new Slideout({
        panel: document.querySelector('.js-page-wrapper'),
        menu: $aside.get(0),
        padding: 290,
        tolerance: 70,
        side: 'right'
      });

      $button.on('click', function () {
        menu.open();
      });

      $('.js-page-wrapper').on('click', function (event) {
        if (menu.isOpen() && !$(event.target).closest('.js-filter-button').length) {
          menu.close();
        }
      });
    }
  });

  $(document).on('click', '.page-header__menu-toggle', function () {
    $('.page').toggleClass('is-menu is-blur is-locked');
  });

  $('.js-search-toggle').on('click', function() {
    $('.page').removeClass('is-menu is-blur');
    $('.page').toggleClass('is-locked');
  });

  $('.page-header__menu-toggle').on('click', function() {
    $('.page').removeClass('is-search');
    $('.page').toggleClass('is-locked');
  });


  $('.js-tabs').each(function () {
    const $tabs = $(this),
          $menu = $tabs.find('.js-tabs-button'),
          $items = $tabs.find('.js-tabs-item');

    $menu.on('click', function () {
      const $button = $(this),
            id = $button.index();

      $button.addClass('is-active').siblings().removeClass('is-active');
      $items.eq(id).addClass('is-active').siblings().removeClass('is-active');
    });
  });

  $('.js-main-aside-sticky').stick_in_parent({
    offset_top: pageHeaderHeight
  });

  $('.js-aside-sticky').stick_in_parent({
    offset_top: pageHeaderHeight + 30
  });

 $('.js-tabs-nav').on('click', function () {
    $(this).addClass('is-active').siblings().removeClass('is-active');

    $('.section-layout__tab').eq($(this).index()).addClass('is-active').siblings().removeClass('is-active');

    return false;
  });


  $('.js-box').each(function() {
    const $box = $(this),
          $boxNav = $box.find('.js-box-nav'),
          $list = $boxNav.find('.js-box-nav-list'),
          $dropdown = $boxNav.find('.js-box-nav-dropdown'),
          $more = $boxNav.find('.js-box-nav-more'),
          $toggle = $boxNav.find('.js-box-nav-toggle'),
          $tab = $box.find('.js-box-tab'),
          listWidth = $list.width();

    let itemsWidth = 0;

    $list.children().each(function() {
      const $item = $(this),
            index = $item.index(),
            itemWidth = $item.width();

      itemsWidth += itemWidth;

      if (itemsWidth > listWidth) {
        $more.css({
          opacity: 1,
          visibility: 'visible'
        });
        $item.appendTo($dropdown);
      }

      $item.on('click', function() {
        $item.addClass('is-active').siblings().removeClass('is-active');
        $tab.eq(index).addClass('is-active').siblings().removeClass('is-active');
      });
    });

    $toggle.on('click', function() {
      $boxNav.toggleClass('is-opened');
    });
  });

  $(".levels__column").mouseenter(function(){
    var curDiv = $( ".levels__column" );

    $(this).addClass('cur')
        .prevAll()
        .addClass('prev');

      curDiv.nextAll()
        .addClass('next');
  });

   $(".levels__column").mouseleave(function(){
    var curDiv = $( ".levels__column" );

    curDiv.removeClass('cur prev next');
  })

  $(document).on('click', function (event) {
    if (!$(event.target).closest('.js-box-nav-toggle').length) {
      $('.js-box-nav').removeClass('is-opened');
    }
  });

  const $videoCards = $('.js-video-card');

  $videoCards.each(function(index) {
    const $card = $(this),
          $img = $card.find('.js-video-card-img'),
          id = getIdByUrl($card.attr('href'));

    if (id) {
      const imgSrc = getImgById(id);
      $img.attr('src', imgSrc);

      $card.on('click', (event) => {
        event.preventDefault();

        let prevId = index === 0 ? $videoCards.length - 1 : index - 1;
        let nextId = index === $videoCards.length - 1 ? 0 : index + 1;

        let template = `
          <div class="modal-video">
            <div class="modal-video__header">
              <button class="modal-video__close" data-fancybox-close></button>
            </div>
            <div class="modal-video__body">
              <iframe class="modal-video__iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src="https://www.youtube-nocookie.com/embed/${id}?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=transparent&enablejsapi=1&html5=1" scrolling="no"></iframe>
            </div>
            <div class="modal-video__footer">
              <span class="modal-video__link is-prev js-video-link" data-id="${prevId}">Предыдущий термин</span>
              <span class="modal-video__link is-next js-video-link" data-id="${nextId}">Следующий термин</span>
            </div>
          </div>
        `;

        if ($card.data('nav') == false) {
          template = `
            <div class="modal-video">
              <div class="modal-video__header">
                <button class="modal-video__close" data-fancybox-close></button>
              </div>
              <div class="modal-video__body">
                <iframe class="modal-video__iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src="https://www.youtube-nocookie.com/embed/${id}?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=transparent&enablejsapi=1&html5=1" scrolling="no"></iframe>
              </div>
            </div>
          `
        }

        $.fancybox.open(template,
          {
            // arrows: false,
            infobar: false,
            smallBtn: false,
            toolbar: false
          }
        )
      });
    }
  });

  $(document).on('click', '.js-video-link', function () {
    $.fancybox.close();
    $videoCards.eq($(this).data('id')).trigger('click');
  });

  function getIdByUrl(url) {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    const match = url.match(regExp);
    return (match && match[7].length === 11)? match[7] : false;
  }

  function getImgById(id) {
    if (!id) return false;
    return `https://img.youtube.com/vi/${id}/maxresdefault.jpg`
  }

  $('.js-video').fancybox({
    iframe: {
      tpl:
        `
          <iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" allowfullscreen allow="autoplay; fullscreen" src=""></iframe>
        `
    }
  });


  $('.js-main-body-module').each(function(){
    let $this = $(this),
        $btn = $this.find('.js-mobile-filter-btn'),
        $filter = $this.find('.js-new-mobile-filter'),
        $header = $this.find('.main-body__module-header'),
        $drop = $this.find('.main-body__grid');

    $btn.on('click', function(){
      $filter.toggleClass('is-active');
      $(this).toggleClass('is-active');
      $('.js-main-aside-sticky').removeClass('is_stuck');
      $('.js-main-aside-sticky').css('position', 'static');
      $('.js-main-aside-sticky').css('width', '100%');
      return false;
    });

    $header.on('click', function(){
      $this.toggleClass('is-opened');
    });
  });

  $('.graphic-tabs').each(function(){
    var $this = $(this),
        $btn = $this.find('.graphic-tabs__button'),
        $tab = $this.find('.graphic-tabs__tab');

    $btn.on('click', function () {
      $(this).addClass('is-active').siblings().removeClass('is-active');
  
      $tab.eq($(this).index()).addClass('is-active').siblings().removeClass('is-active');
    });
  });

  var maxBarThickness = 20;
  var fontSize = 8;
  var padding = 35;
  var minRotation = 90;
  var desktopColor = 'transparent';

  var aspectRatio = 0.85;
  // var aspectRatio = 2;

  if ($(window).width() >= 480) {
    aspectRatio = 0.8;
  }

  if ($(window).width() >= 640) {
    aspectRatio = 1.5;
    padding = 20;
    fontSize = 13;
  }

  if ($(window).width() >= 768) {
    aspectRatio = 1.75;
    padding = 20;
  }

  if ($(window).width() >= 1024) {
    aspectRatio = 3;
    padding = 60;
  }

  if ($(window).width() >= 1260) {
    minRotation = 0;
    padding = 60;
  }

  $(window).resize(function() {
    if ($(window).width() >= 480) {
      aspectRatio = 0.8;
    }
  
    if ($(window).width() >= 640) {
      aspectRatio = 1.5;
      padding = 60;
      fontSize = 13;
    }
  
    if ($(window).width() >= 768) {
      aspectRatio = 1.75;
    }
  
    if ($(window).width() >= 1024) {
      aspectRatio = 3;
    }
  
    if ($(window).width() >= 1260) {
      minRotation = 0;
    }
  });

  // start investment
  var $charts = $('.js-investment-chart');

  if ($charts.length) {

    Chart.elements.Rectangle.prototype.draw = function() {
    
      var ctx = this._chart.ctx;
      var vm = this._view;
      var left, right, top, bottom, signX, signY, borderSkipped, radius;
      var borderWidth = vm.borderWidth;
      // Set Radius Here
      // If radius is large enough to cause drawing errors a max radius is imposed
      var cornerRadius = 5;
  
      if (!vm.horizontal) {
          // bar
          left = vm.x - vm.width / 2;
          right = vm.x + vm.width / 2;
          top = vm.y;
          bottom = vm.base;
          signX = 1;
          signY = bottom > top? 1: -1;
          borderSkipped = vm.borderSkipped || 'bottom';
      } else {
          // horizontal bar
          left = vm.base;
          right = vm.x;
          top = vm.y - vm.height / 2;
          bottom = vm.y + vm.height / 2;
          signX = right > left? 1: -1;
          signY = 1;
          borderSkipped = vm.borderSkipped || 'left';
      }
  
      // Canvas doesn't allow us to stroke inside the width so we can
      // adjust the sizes to fit if we're setting a stroke on the line
      if (borderWidth) {
          // borderWidth shold be less than bar width and bar height.
          var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
          borderWidth = borderWidth > barSize? barSize: borderWidth;
          var halfStroke = borderWidth / 2;
          // Adjust borderWidth when bar top position is near vm.base(zero).
          var borderLeft = left + (borderSkipped !== 'left'? halfStroke * signX: 0);
          var borderRight = right + (borderSkipped !== 'right'? -halfStroke * signX: 0);
          var borderTop = top + (borderSkipped !== 'top'? halfStroke * signY: 0);
          var borderBottom = bottom + (borderSkipped !== 'bottom'? -halfStroke * signY: 0);
          // not become a vertical line?
          if (borderLeft !== borderRight) {
              top = borderTop;
              bottom = borderBottom;
          }
          // not become a horizontal line?
          if (borderTop !== borderBottom) {
              left = borderLeft;
              right = borderRight;
          }
      }
  
      ctx.beginPath();
      ctx.fillStyle = vm.backgroundColor;
      ctx.strokeStyle = vm.borderColor;
      ctx.lineWidth = borderWidth;
  
      // Corner points, from bottom-left to bottom-right clockwise
      // | 1 2 |
      // | 0 3 |
      var corners = [
          [left, bottom],
          [left, top],
          [right, top],
          [right, bottom]
      ];
  
      // Find first (starting) corner with fallback to 'bottom'
      var borders = ['bottom', 'left', 'top', 'right'];
      var startCorner = borders.indexOf(borderSkipped, 0);
      if (startCorner === -1) {
          startCorner = 0;
      }
  
      function cornerAt(index) {
          return corners[(startCorner + index) % 4];
      }
  
      // Draw rectangle from 'startCorner'
      var corner = cornerAt(0);
      ctx.moveTo(corner[0], corner[1]);
  
      for (var i = 1; i < 4; i++) {
          corner = cornerAt(i);
          var nextCornerId = i+1;
          if(nextCornerId == 4){
              nextCornerId = 0
          }
  
          var nextCorner = cornerAt(nextCornerId);
  
          var width = corners[2][0] - corners[1][0];
          var height = corners[0][1] - corners[1][1];
          var x = corners[1][0];
          var y = corners[1][1];
          
          var radius = cornerRadius;
          
          // Fix radius being too large
          if(radius > height/2){
              radius = height/2;
          }if(radius > width/2){
              radius = width/2;
          }
  
          ctx.moveTo(x + radius, y);
          ctx.lineTo(x + width - radius, y);
          ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
          ctx.lineTo(x + width, y + height - radius);
          ctx.quadraticCurveTo(x + width, y + height, x + width, y + height + 7);
          ctx.lineTo(x + radius, y + height);
          ctx.quadraticCurveTo(x, y + height, x, y + height + 7);
          ctx.lineTo(x, y + radius);
          ctx.quadraticCurveTo(x, y, x + radius, y);
  
      }
  
      ctx.fill();
      if (borderWidth) {
          ctx.stroke();
      }
  }; 
    
    // axios.get('/api/investment').then(function(response) {
    //   var data = response.data,
    //       labels = [],
    //       totalNumber = [],
    //       privateInvest = [],
    //       publicInvest = [],
    //       totalInvest = [];

    //   for (var key in data) {
    //     labels.push(key);
    //     totalNumber.push(data[key].total_number_projects);
    //     privateInvest.push(data[key].private_investment);
    //     publicInvest.push(data[key].public_investment);
    //     totalInvest.push(data[key].total_investment);
    //   }
      
    //   $charts.each(function() {
    //     var $chart = $(this);

    //     chartInit($chart, {
    //       labels,
    //       totalNumber,
    //       privateInvest,
    //       publicInvest,
    //       totalInvest
    //     });


    //     var resize = false;

    //     $(window).resize(function() {
    //       if (resize) {
    //         setTimeout(function() {
    //           chartInit($chart, {
    //             labels,
    //             totalNumber,
    //             privateInvest,
    //             publicInvest,
    //             totalInvest
    //           });
    //         }, 2000);
    //       }
    //     });

    //   });
    // });
  }
  // end investment

  
  function chartInit($chart, data) {
    var isMobile = $chart.hasClass('is-mobile'),
        ratioMobile = 384 / $(window).width();

    if (isMobile || $(window).width() >= 640) {
      desktopColor = '#F2F3F6';
    }

    data = {
      labels: ['2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029'],
      totalInvest: [1200, 1100, 1350, 1100, 1600, 1550, 1600, 1500, 1350, 1550, 2150, 1800, 2000, 1900, 2250],
      totalNumber: [1500, 1550, 1700, 1800, 1740, 1700, 1760, 1740, 1600, 1900, 1800, 2100, 2200, 2250, 2400]
    }

    var chart = new Chart($chart, {
      type: 'bar',
      data: {
        labels: data.labels,
        datasets: [{
          type: 'line',
          label: 'Объем инвестиций в проектах ГЧП',
          data: data.totalNumber,
          fill: false,
          backgroundColor: '#1A295B',
          borderColor: '#1A295B',
          borderWidth: 1,
          lineTension: 0,
          pointRadius: 4.5,
          pointBorderColor: '#FFF',
          pointBorderWidth: 2,
          yAxisID: 'y-axis-1'
        }, {
          type: 'bar',
          label: 'Бюджетные инвестиции в долларах',
          data: data.totalInvest,
          backgroundColor: '#B52528'
        }]
      },
      options: {
        aspectRatio: isMobile ? ratioMobile : aspectRatio,
        legend: {
          display: false
        },
        datasets: {
          bar: {
            maxBarThickness: maxBarThickness
          }
        },
        scales: {
          xAxes: [{
            stacked: true,
            gridLines: {
              color: 'transparent',
              zeroLineColor: 'transparent'
            },
            ticks: {
              fontColor: '#919AB5',
              fontFamily: 'SFUIDisplay, sans-serif',
              fontSize: fontSize,
              padding: 15,
              maxRotation: 0
            }
          }],
          yAxes: [{
            position: 'left',
            stacked: true,
            ticks: {
              display: !isMobile,
              beginAtZero: true,
              callback: function (tickValue, index, ticks) {
                return `${parseInt(tickValue).toLocaleString()}`;
              },
              fontColor: '#919AB5',
              fontFamily: 'SFUIDisplay, sans-serif',
              fontSize: fontSize,
              padding: padding,
            },
            gridLines: {
              color: desktopColor,
              zeroLineColor: desktopColor,
              drawOnChartArea: false,
              tickMarkLength: 0
            }
          }, {
            position: 'right',
            ticks: {
              display: !isMobile,
              beginAtZero: true,
              callback: function (tickValue, index, ticks) {
                return parseInt(tickValue).toLocaleString();
              },
              fontColor: '#919AB5',
              fontFamily: 'SFUIDisplay, sans-serif',
              fontSize: fontSize,
              padding: padding,
              // minRotation: -minRotation
            },
            gridLines: {
              color: desktopColor,
              zeroLineColor: desktopColor,
              tickMarkLength: 0
            }
          }]
        },
        tooltips: {
          // Disable the on-canvas tooltip
          enabled: false,

          custom: function(tooltipModel) {
              // Tooltip Element
              var tooltipEl = document.getElementById('chartjs-tooltip');

              // Create element on first render
              if (!tooltipEl) {
                  tooltipEl = document.createElement('div');
                  tooltipEl.id = 'chartjs-tooltip';
                  tooltipEl.innerHTML = '<table></table>';
                  document.body.appendChild(tooltipEl);
              }

              // Hide if no tooltip
              if (tooltipModel.opacity === 0) {
                  tooltipEl.style.opacity = 0;
                  return;
              }

              // Set caret Position
              tooltipEl.classList.remove('above', 'below', 'no-transform');
              if (tooltipModel.yAlign) {
                  tooltipEl.classList.add(tooltipModel.yAlign);
              } else {
                  tooltipEl.classList.add('no-transform');
              }

              function getBody(bodyItem) {
                  return bodyItem.lines;
              }

              // Set Text
              if (tooltipModel.body) {
                  var titleLines = tooltipModel.title || [];
                  var bodyLines = tooltipModel.body.map(getBody);

                  var innerHtml = '<thead>';
                  innerHtml += '</thead><tbody>';

                  bodyLines.forEach(function(body, i) {
                      var year = titleLines[0],
                          index = data.labels.indexOf(year);

                      innerHtml += '<tr><td>';

                      innerHtml += '<span class="title">'+ titleLines[i] +'</span>';

                      innerHtml += '<div class="props">';
                        innerHtml += '<div class="prop">';
                          innerHtml += '<span class="label"><span class="dot" style="background: #B52528;"></span><span class="inner">Бюджетные инвестиции в долларах</span></span>';
                          innerHtml += '<span class="value">'+ parseFloat(data.totalInvest[index]).toLocaleString() +' млрд.₽ </span>';
                        innerHtml += '</div>';
                       
                        innerHtml += '<div class="prop">';
                          innerHtml += '<span class="label"><span class="dot" style="background: #23346B;"></span><span class="inner">Объем инвестиций в проектах ГЧП</span></span>';
                          innerHtml += '<span class="value">'+ parseFloat(data.totalNumber[index]).toLocaleString() +' млрд.₽ </span>';
                        innerHtml += '</div>';
                      innerHtml += '</div>';

                      innerHtml += '</td></tr>'
                  });
                  innerHtml += '</tbody>';

                  var tableRoot = tooltipEl.querySelector('table');
                  tableRoot.innerHTML = innerHtml;
              }

              // `this` will be the overall tooltip
              var position = this._chart.canvas.getBoundingClientRect();

              // Display, position, and set styles for font
              tooltipEl.style.opacity = 1;
              tooltipEl.style.position = 'absolute';
              tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - 375 + 'px';
              tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 25 + 'px';
              tooltipEl.style.fontFamily = 'SFUIDisplay';
              tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
              tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
              tooltipEl.style.padding = '20px ' + '25px';
              tooltipEl.style.pointerEvents = 'none';
          }
        },
      }
    });

    var $legends = $('.js-investment-chart-legend');
    $legends.html(chart.generateLegend());
    return chart;
  }

 

  $charts.each(function(){
    chartInit($(this), {});
  });


  var $doughts = $('.js-graphic-bar');

  if ($doughts.length) {
    $doughts.each(function (i) {
      var $dought = $(this);
      var $chart = $dought.find('.js-graphic-bar-item');
      var $list = $dought.find('.js-graphic-bar-list');

      var chart = new Chart($chart, {
        type: 'doughnut',
        data: {
          datasets: [{
            data: [45, 35, 20],
            label: 'By sphere',
            backgroundColor: ['#23346B', '#9199B5', '#B52528']
          }],
          labels: ['Муниципальный уровень', 'Региональный уровень', 'Федеральный / национальный уровень']
        },
        options: {
          responsive: true,
          legend: {
            display: false
          },
          tooltips: {
            // Disable the on-canvas tooltip
            enabled: false,

            custom: function(tooltipModel) {
                var total = 10000;

                // Tooltip Element
                var tooltipEl = document.getElementById('chartjs-tooltip');

                // Create element on first render
                if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    tooltipEl.innerHTML = '<table></table>';
                    document.body.appendChild(tooltipEl);
                }

                // Hide if no tooltip
                if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                }

                // Set caret Position
                tooltipEl.classList.remove('above', 'below', 'no-transform');
                if (tooltipModel.yAlign) {
                    tooltipEl.classList.add(tooltipModel.yAlign);
                } else {
                    tooltipEl.classList.add('no-transform');
                }

                function getBody(bodyItem) {
                    return bodyItem.lines;
                }

                // Set Text
                if (tooltipModel.body) {
                    var titleLines = tooltipModel.title || [];
                    var bodyLines = tooltipModel.body.map(getBody);

                    var innerHtml = '<thead>';

                    titleLines.forEach(function(title) {
                        innerHtml += '<tr><th>' + title + '</th></tr>';
                    });
                    innerHtml += '</thead><tbody>';

                    bodyLines.forEach(function(body, i) {
                        var arr = body[0].split(': ');
                        var label = arr[0],
                            value = arr[1];

                        innerHtml += '<tr><td>';

                        innerHtml += '<span class="title">'+ label +'</span>';

                        if (label === 'Other') {
                            innerHtml += '<ul class="list">';
                                innerHtml += '<li>Defense and security of the country</li>';
                                innerHtml += '<li>Agriculture and hunting infrastructure</li>';
                                innerHtml += '<li>Housing construction</li>';
                            innerHtml += '</ul>';
                        }

                        var isProjects = $('.graphic-tabs__button.is-active').index() === 0;

                        innerHtml += '<div class="props">';
                            innerHtml += '<div class="prop">';
                            innerHtml += '<span class="label"><span class="inner">Всего проектов</span></span>';
                            innerHtml += '<span class="value">'+ parseInt(value).toLocaleString() +' проектов</span>';
                            innerHtml += '</div>';

                            innerHtml += '<div class="prop">';
                            innerHtml += '<span class="label"><span class="inner">Общий объём инвестиций</span></span>';
                            innerHtml += '<span class="value">'+ (value / total * 100).toFixed(1) +' млрд.₽ </span>';
                            innerHtml += '</div>';
                            
                            innerHtml += '<div class="prop">';
                            innerHtml += '<span class="label"><span class="inner">Объём частных инвестиций</span></span>';
                            innerHtml += '<span class="value">'+ (value / total * 100).toFixed(1) +' млрд.₽ </span>';
                            innerHtml += '</div>';
                        innerHtml += '</div>';

                        innerHtml += '</td></tr>'
                    });
                    innerHtml += '</tbody>';

                    var tableRoot = tooltipEl.querySelector('table');
                    tableRoot.innerHTML = innerHtml;
                }

                // `this` will be the overall tooltip
                var canvas = this._chart.canvas;
                var position = canvas.getBoundingClientRect();

                // Display, position, and set styles for font
                tooltipEl.style.opacity = 1;
                tooltipEl.style.position = 'absolute';
                tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - position.width - 75 + 'px';
                tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 20 + 'px';
                tooltipEl.style.fontFamily = 'SFUIDisplay';
                tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                tooltipEl.style.padding = '20px ' + '25px';
                tooltipEl.style.pointerEvents = 'none';
            }
          },

          onHover: function (event, activeElements) {
            if (activeElements.length) {
                var datasetIndex = activeElements[0]._datasetIndex;
                var index = activeElements[0]._index;
                var activeDataset = this.data.datasets[datasetIndex];

                for (var i = 0; i < activeDataset.backgroundColor.length; i++) {
                    var arr = activeDataset.backgroundColor[i].split(','),
                        alpha = arr[arr.length - 1],
                        color = '';

                    if (i === index) {
                        if (alpha.indexOf(' 0.3)') > -1) {
                            alpha = ' 1)';

                            arr[arr.length - 1] = alpha;
                        }
                    } else {
                        if (alpha.indexOf('1)') > -1) {
                            alpha = ' 0.3)';

                            arr[arr.length - 1] = alpha;
                        }
                    }

                    color = arr.join();
                    activeDataset.backgroundColor[i] = color;
                }
            } else {
                var $tooltip = $('#chartjs-tooltip');

                var data = this.config.data.datasets[0];

                for (var i = 0; i < data.backgroundColor.length; i++) {
                    var arr = data.backgroundColor[i].split(','),
                        alpha = arr[arr.length - 1],
                        color = '';

                    if (alpha.indexOf(' 0.3)') > -1) {
                        alpha = ' 1)';

                        arr[arr.length - 1] = alpha;
                    }

                    color = arr.join();
                    data.backgroundColor[i] = color;

                    $tooltip.css({
                        opacity: 0
                    });
                }
            }
            
            this.update();
          }
        }
      });

      $list.html(chart.generateLegend());
    });
  }




  $('.graphic-button__count .js-count').each(function() {
    var $count = $(this),
        numb = parseInt($count.html()) || 0;

    $count.html(numb.toLocaleString());
  });

  $('.js-wiget-copy').each(function(){
    var $this = $(this),
        $link = $this.find('.js-copy-text'),
        $btn = $this.find('.js-copy-button'),
        $value = $link.text(),
        $alert = $this.find('.js-copy-alert');

    function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
    }

    $btn.on("click", function() {
      copyToClipboard("#link-val");
      $alert.fadeIn("slow");
      setTimeout(function() { 
        $alert.fadeOut('slow'); 
      }, 2000);
    });
    
  });


  // $(document).on('click', '.js-search-toggle', function () {
  //   menu.open();
  // });


  // var $slider = $('.js-search-investment-slider'),
  //   $sliderMin = $('.js-search-investment-min'),
  //   $sliderMax = $('.js-search-investment-max');
  //
  // if ($slider.length) {
  //   var range = noUiSlider.create($slider[0], {
  //     start: [0, 250000000],
  //     connect: true,
  //     range: {
  //       min: 0,
  //       max: 500000000
  //     }
  //   });
  //
  //   range.on('update', function (values) {
  //     var min = parseInt(values[0]).toLocaleString() + ' тыс руб';
  //     var max = parseInt(values[1]).toLocaleString() + ' тыс руб';
  //
  //     $sliderMin.html(min);
  //     $sliderMax.html(max);
  //   });
  //
  //   $('.js-search-investment-apply').on('click', function (event) {
  //     event.preventDefault();
  //
  //     var values = range.get();
  //     var min = parseInt(values[0]).toLocaleString();
  //     var max = parseInt(values[1]).toLocaleString();
  //
  //     var $itemSelected = $('<div/>').addClass('search-label js-search-label').html(min + ' - ' + max + ' тыс руб');
  //     $('.js-super-search').find('.js-super-search-selected').append($itemSelected);
  //   });
  // }



  // $(document).on('click', '.page-header__menu-toggle', function () {
  //   if ($('.page').hasClass('is-locked')) {
  //     $('.page').toggleClass('is-menu is-blur');
  //   } else {
  //     $('.page').toggleClass('is-menu is-blur is-locked');
  //   }
  // });


  // new Swiper('.js-article-slider', {
  //   wrapperClass: 'js-slider-wrapper',
  //   slideClass: 'js-slider-slide',
  //   breakpointsInverse: true,
  //   navigation: {
  //     prevEl: '.js-slider-prev',
  //     nextEl: '.js-slider-next'
  //   },
  //   breakpoints: {
  //     0: {
  //       slidesPerView: 1.2,
  //       spaceBetween: 10
  //     },
  //     640: {
  //       slidesPerView: 2,
  //       spaceBetween: 20
  //     },
  //     1024: {
  //       slidesPerView: 1,
  //       spaceBetween: 0
  //     }
  //   }
  // })

  $('.js-limit-area').each(function() {
    var $this = $(this),
        $truncateSection = $this.find('.js-limit-area-main'),
        $btn = $this.find('.js-limit-area-toggle'),
        $btnText = $btn.html(),
        maxHeight = parseInt($truncateSection.css('max-height'));

    $btn.on('click', function() {
      if ($this.hasClass('is-opened')) {
        $this.removeClass('is-opened');
        $btn.html($btnText);
      } else {
        $this.addClass('is-opened');
        $btn.html('Свернуть <i class="fe fe-chevron-up"></i>');
      }
    });
  });

  /* Filter Dropdown */
  const stickyPageHeaderHeight = $('.page__header').outerHeight();

  $('.js-filter-toggle').on('click', function() {
    $('.page').removeClass('is-menu is-search');
    $('.page').toggleClass('is-filter');

    if ( $('.page').hasClass('is-filter') ) {
      $([document.documentElement, document.body]).animate({
        scrollTop: $(this).offset().top - stickyPageHeaderHeight - 15
      }, 700);

      $('.main-body__container').append('<div class="filter-overlay"></div>');

      $('.filter-overlay').on('click', function() {
        $('.js-filter-toggle').trigger('click');
      });
    } else {
      $('.filter-overlay').remove();
    }
  });

  // $('.moder-card').each(function(){
  //   let $this = $(this),
  //       $photo = $this.find('.moder-card__photo'),
  //       $name = $this.find('.moder-card__name');

  //     $photo.mouseenter(function() {
  //       $name.css('text-decoration', 'underline');
  //     });

  //     $photo.mouseleave(function(){
  //       $name.css('text-decoration', 'none');
  //     });

  //     $name.mouseenter(function() {
  //       $(this).css('text-decoration', 'underline');
  //     });

  //     $name.mouseleave(function(){
  //       $(this).css('text-decoration', 'none');
  //     });
  // })

  

  $('.js-inline-filter').each(function(){
    let $this = $(this),
        $page = $('.page'),
        $drop = $this.find('.js-inline-dropdown'),
        $btn = $this.find('.js-toggle-filter');

    $btn.on('click', function(){
      $page.removeClass('is-menu is-search');
      $page.toggleClass('is-filter');
      $(this).toggleClass('is-active');
      $drop.toggleClass('is-active');

      if($page.hasClass('is-filter')){
        $([document.documentElement, document.body]).animate({
          scrollTop: $(this).offset().top - stickyPageHeaderHeight - 15
        }, 700);

        $('.main-body__container, .articles-layout').append('<div class="js-inline-overlay"></div>');

        $('.js-inline-overlay').on('click', function() {
          $btn.trigger('click');
        });
      }else {
        $('.js-inline-overlay').remove();
      }
    });

  });

  $('.button-sorting').each(function(){
    let $this = $(this),
        $drop = $this.find('.button-sorting__dropdown');
    
    $this.on('click', function(){
      $(this).toggleClass('is-active');
      $drop.toggleClass('is-active');
    });
    
    $(document).mouseup(function (e){
      var div = $('.button-sorting'); 
      if (!div.is(e.target)
          && div.has(e.target).length === 0) { 
          div.removeClass('is-active');
          $drop.removeClass('is-active');
      }
    });
  });


  $('.dropdown-filter__toggle-active-params').on('click', function() {
    const parentNode = $(this).closest('.dropdown-filter__footer')

    parentNode.toggleClass('is-filtered-params-expanded');
  });
  /* Filter Dropdown */

  /* Super-Search */
  $('.super-search--filter').on('click', '.search-menu__title', function() {
    const $current = $(this).closest('.search-menu__item');
    const $body = $('body');

    $('.super-search--filter .search-menu__item').not($current).removeClass('is-active');
    $current.toggleClass('is-active');

    if ( $current.hasClass('is-active') ) {
      $body.addClass('is-dropdown-opened');

      setTimeout(function() {
        
        $(document).on('mousedown.outside', function(e) {
          const $target = $(e.target);
          
          if ( !$target.closest('.search-menu__item').length ) {
            $current.removeClass('is-active');
            $body.removeClass('is-dropdown-opened');
          }
        });

      }, 4)
    } else {
      $body.removeClass('is-dropdown-opened');
      $(document).off('.outside');
    }
  });

  /* calculate checkboxes */
  let filteredCounter = 0;

  $('.super-search--filter').on('click', '.input-control__input', function() {
    const $filteredCounter = $('.super-search--filter .search-menu__amount');

    filteredCounter = getCheckedLength();

    (filteredCounter) 
      ? 
      $filteredCounter.addClass('is-filtered') 
      : 
      $filteredCounter.removeClass('is-filtered');

    $filteredCounter.html(filteredCounter);
  });

  function getCheckedLength() {
    return $('.super-search--filter input[type="checkbox"]:checked').length;
  }
  /* calculate checkboxes */
  /* Super-Search */

  /* Events Carousel */
  let eventsCarouselInstances = [],
      isEnabled = null;

  function enableEventsCarousels() {
    if (!document.querySelector('.js-events-carousel')) return;

    const carousels = document.querySelectorAll('.js-events-carousel');

    carousels.forEach((carousel) => {
      const isPagination = carousel.querySelectorAll('.js-slider-slide').length > 1;
      const pagination = isPagination ? carousel.parentNode.querySelectorAll('.js-slider-pagination') : false;
      
      eventsCarouselInstances.push(
        new Swiper(carousel, {
          slidesPerView: 1.07,
          spaceBetween: 10,
          breakpointInverse: true,
          wrapperClass: 'js-slider-wrapper',
          slideClass: 'js-slider-slide',
          pagination: {
            el: pagination,
            clickable: true
          },
          breakpoints: {
            480: {
              slidesPerView: 1.4,
              spaceBetween: 15
            },
            640: {
              slidesPerView: 2,
              spaceBetween: 17
            }
          }
        })
      );
    });

    isEnabled = true;
  }

  function checkEventsCarousel() {
    if ( $(window).width() < 639 && !isEnabled ) {
      enableEventsCarousels();
    } else if ( $(window).width() > 640 && isEnabled ) {
      eventsCarouselInstances.forEach(instance => {
        instance.destroy(true, true);
      });

      isEnabled = false;
    }

  }

  checkEventsCarousel();
  $(window).on('resize', checkEventsCarousel);
  /* Events Carousel */

  $('.js-sort-by').each(function(item) {
    const self = $(this);
    const trigger = self.find('.js-sort-by-trigger');

    trigger.on('click', function() {
      self.toggleClass('is-opened');

      $(document).on('click', function (e) {
        if ($(e.target).closest('.js-sort-by').length === 0) {
          self.removeClass('is-opened');
        }
      });

    });
  });
});
